<?php /* Template Name: Главная страница */ get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	$currtime=date('U', time());
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="block1" class="js-pad-top" style="background:url(<?= (!empty($pthumb)) ? $pthumb : $options['tpldir'].'/assets/img/bg/bg_top_1.jpg'?>) top / cover no-repeat">
				<div class="container-fuild">
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-md-6  ">
									<div class="texts">
										<p class="h1">
											<?= (empty($pmeta[$prefix.'title'][0])) ? $ttl : $pmeta[$prefix.'title'][0]; ?>
										</p>
										<?php the_content(); ?>
									</div>
									<a href="#" target="_blank" class="btn btn-purple-2 popup-btn">Забронировать путевку</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="block2">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<?php
							$args=array(
								'post_type'=>'shift',
								'posts_per_page'=>-1,
								'meta_key'=>$prefix.'shift_date_in',
								'orderby'=>'meta_value',
								'order'=>'asc'
							);
							$shifts=new WP_Query($args);
							if ($shifts->have_posts()): while ($shifts->have_posts()) : $shifts->the_post();
								get_template_part('loop', 'shift');
							endwhile;
							else : ?>
								<p class="h1">Не найдено ни одной смены!<br>Добавьте смены срочно!</p>
							<?php endif;
							wp_reset_query();
							?>
							<style>.iconsvg svg{width:70px;height:70px}.iconsvg svg path{fill:#fff}.iconsvg svg:hover path{fill:#000}</style>
						</div>
					</div>
				</div>
			</section>
			<section id="block3">
				<div class="container">
					<h1>Ближайшие смены</h1>
					<div class="row">
						<?php
						$twop_i=0;
						if ($shifts->have_posts()): while ($shifts->have_posts()) : $shifts->the_post();
							$sid=$post->ID;
							$smeta=get_post_meta($sid, '', false);
							if($currtime<$smeta[$prefix.'shift_date_in'][0]&&$twop_i<=1){ ?>
								<div class="col-xs-12 col-md-6">
									<h2><?=$post->post_title?></h2>
									<p class="date"><?=strtr(date('d M', $smeta[$prefix.'shift_date_in'][0]), $options['translate']).' — '.strtr(date('d M', $smeta[$prefix.'shift_date_out'][0]), $options['translate'])?></p>
									<?=$smeta[$prefix.'maintext'][0]?>
									<div class="bott">
										<div class="bot-cont">
											<p class="place">
												<span class="digit"><?=$smeta[$prefix.'left_places'][0]?></span>
												<span class="letter">мест <br> осталось</span>
											</p>
										</div>
										<div class="bot-cont">
											<a href="#" target="_blank" class="btn btn-red popup-btn" data-shiftttl="<?=$post->post_title?>">Забронировать путевку</a>
										</div>
									</div>
								</div>
							<?php
							$twop_i++;
							}
						endwhile;
						else : ?>
							<p class="h1">Не найдено ни одной смены!<br>Добавьте смены срочно!</p>
						<?php endif;
						wp_reset_query();
						?>
					</div>
				</div>
			</section>
			<section id="block4">
				<div class="container">
					<h1>
						Преимущества
					</h1>
					<div class="adv-loop">
						<ul>
							<?php
							$adv=maybe_unserialize($pmeta[$prefix.'advantages'][0]);
							// print_r($adv);
							if(!empty($adv)){
								foreach ($adv as $val) { ?>
									<li>
										<?php if(empty($val['icon-img'])){ ?>
										<p class="icon <?=$val['icon']?><?= ($val['icon']=='icon-scene') ? ' lt' : null; ?>"></p>
										<?php } else {
											printf('<img class="b-lazy" src="%s" data-src="%s" alt="%s">',
												$options['tpldir'].'/assets/img/loader.gif',
												$smeta[$prefix.'icon'][0],
												$val['title']
											);
										} ?>
										<p class="discr"><?=$val['title']?></p>
									</li>
								<?php }
							}
							?>
						</ul>
						<?=$pmeta[$prefix.'adv-descr'][0]?>
					</div>
				</div>
			</section>
			<?php get_template_part( 'assets/php/blocks/block', 'video' ); ?>
			<section id="block6">
				<div class="container">
					<h1>Питание</h1>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<p class="">
								<?=(!empty($pmeta[$prefix.'food0'][0]))?$pmeta[$prefix.'food0'][0]:'Еда в лагере вкусная и полезная: меню в соответствии с современными требованиями диетологии и санитарными нормами составляют заведующий производством и шеф-повар.'?>
							</p>
							<p class="">
								<?=(!empty($pmeta[$prefix.'food1'][0]))?$pmeta[$prefix.'food1'][0]:'<span>Питание в лагере пятиразовое:</span><br>	завтрак, обед, полдник, ужин, второй ужин'?>
							</p>
							<p class="">
								<?=(!empty($pmeta[$prefix.'food2'][0]))?$pmeta[$prefix.'food2'][0]:'Интервал между основными приемами пищи — <br>4 часа.'?>
							</p>
							<a href="<?php echo (!empty($pmeta[$prefix.'file_menu'][0])) ? $pmeta[$prefix.'file_menu'][0] : $options['tpldir'].'/assets/files/menu.pdf'?>" target="_blank" class="btn btn-red">Скачать меню</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<img src="<?=$options['tpldir']?>/assets/img/loader.gif" data-src="<?=$options['tpldir']?>/assets/img/eat.jpg" alt="girl" class="g b-lazy">
						</div>
						<div class="col-xs-12 col-md-4">
							<p style="margin-bottom:31px;width: 90%;">
								<?=(!empty($pmeta[$prefix.'food3'][0]))?$pmeta[$prefix.'food3'][0]:'В ресторане установлена шведская линия питания, которая позволит ребенку выбрать блюдо, которое подходит по вкусовым и диетическим предпочтениям именно ему.'?>
							</p>
							<p class="">
								<?=(!empty($pmeta[$prefix.'food4'][0]))?$pmeta[$prefix.'food4'][0]:'Каждое блюдо повторяется в меню не чаще, чем раз в полторы недели, а фрукты и овощи подают каждый день.'?>
							</p>
						</div>
					</div>
				</div>
			</section>
			<section id="block7">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-sm-offset-6 col-md-5 col-md-offset-7 text">
							<h1>проживание</h1>
						</div>
					</div>
				</div>
				<?php if(!empty($pmeta[$prefix.'living_text'][0])){ ?>
				<div class="container-fuild">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-7">
								<img src="<?=$options['tpldir']?>/assets/img/loader.gif" data-src="<?= (!empty($pmeta[$prefix.'living_img_id'][0])) ? wp_get_attachment_image_src( $pmeta[$prefix.'living_img_id'][0], 'main-living-img' ) : $options['tpldir'].'/assets/img/livedecor.png'; ?>" alt="" class="dec b-lazy">
							</div>
							<div class="col-xs-12 col-sm-6 col-md-5 text">
								<?=$pmeta[$prefix.'living_text'][0]?>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(!empty($pmeta[$prefix.'every-room'][0])||!empty($pmeta[$prefix.'every-room-also'][0])||!empty($pmeta[$prefix.'every-room-parents'][0])){ ?>
				<div class="container pad">
					<div class="row">
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="cont">
								<p class="bold">В номере есть:</p>
								<ul>
									<?php
									$list=maybe_unserialize( $pmeta[$prefix.'every-room'][0] );
									foreach ($list as $key => $val) {
										echo '<li><p>'.$val.'</p></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="cont">
								<p class="bold">А также:</p>
								<ul>
									<?php
									$list=maybe_unserialize( $pmeta[$prefix.'every-room-also'][0] );
									foreach ($list as $key => $val) {
										echo '<li><p>'.$val.'</p></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="cont">
								<p class="bold"> <i class="icon-warning"></i> Родителям на заметку:</p>
								<ul>
									<?php
									$list=maybe_unserialize( $pmeta[$prefix.'every-room-parents'][0] );
									foreach ($list as $key => $val) {
										echo '<li><p>'.$val.'</p></li>';
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php } ?>
			<?php get_template_part( 'assets/php/blocks/block', 'schedule' ); ?>
			<?php get_template_part( 'assets/php/blocks/block', 'trainers' ); ?>
			<?php get_template_part( 'assets/php/blocks/block', 'docs' ); ?>
			<?php get_template_part( 'assets/php/blocks/block', 'photo' ); ?>
			<?php get_template_part( 'assets/php/blocks/block', 'review' ); ?>
			<section id="block13">
				<div class="container">
					<div class="col-xs-12 col-md-8">
						<h1>Трансфер</h1>
						<?php if(empty($pmeta[$prefix.'transfer_map_id'][0])){ ?>
						<img src="<?=$options['tpldir']?>/assets/img/loader.gif" data-src="<?=$options['tpldir']?>/assets/img/ukrain.png" class="b-lazy">
						<?php } else {
							printf('<img src="%s" data-src="%s" class="b-lazy">',
								$options['tpldir'].'/assets/img/loader.gif',
								wp_get_attachment_image_src( $pmeta[$prefix.'transfer_map_id'][0], 'full', false )[0]
							);
						} ?>
					</div>
					<div class="col-xs-12 col-md-4">
						<?php
						if(!empty($pmeta[$prefix.'transfer_text'][0])){
							echo $pmeta[$prefix.'transfer_text'][0];
							echo (!empty($pmeta[$prefix.'transfer_file'][0])) ? '<a href="'.$pmeta[$prefix.'transfer_file'][0].'" class="openmodal btn btn-purple test">Цены на трансфер</a>' : null;
							echo (!empty($pmeta[$prefix.'transfer_file_descr'][0])) ? '<p class="qut">'.$pmeta[$prefix.'transfer_file_descr'][0].'</p>' : null;
						} ?>
					</div>
				</div>
			</section>
			<section id="map"></section>
		<?php endwhile; ?>

		<?php else: ?>
			<section>
				<article>
					<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
				</article>
			</section>
		<?php endif; ?>
	</main>

<?php get_footer(); ?>
