<?php get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	$mainimg=wp_get_attachment_image($pmeta['_thumbnail_id'][0], 'full', false, array('class'=>'bg', 'alt'=>$ttl));
	$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="aboutus-top">
			<?php
			if(!empty($mainimg)){
				echo remove_width_attribute($mainimg);
			} else { ?>
				<img src="<?=$options['tpldir']?>/assets/img/about_bg.jpg" class="bg">
			<?php }	?>
				<div class="container js-pad-top abcont">
					<div class="row">
						<div class="col-xs-12 col-sm-6 a-shap">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</section>
			<div class="stext">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="container">
						<?php the_content(); ?>
						<?php edit_post_link(); ?>
					</div>
				</article>
			</div>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
	</main>
<?php get_footer(); ?>
