<?php
global $options;
$phid=$post->ID;
$ttl=get_the_title();
$prefix='startimecamp_';
$phmeta=get_post_meta($phid, '', false);
$phthumb=wp_get_attachment_image_src($phmeta['_thumbnail_id'][0], 'photo-loop-img');
$images=array();
foreach (maybe_unserialize( $phmeta[$prefix.'mediaimages'][0] ) as $key => $value) {
	$images[]=$key;
}
// print_r($phmeta);
?>
<div class="media_ell phid-<?=$phid?>">
	<!-- <a href="#" class="media_link js-media_link" target="_blank" data-imgs="<?=implode(',', $images)?>"></a> -->
	<a href="<?=get_the_permalink()?>" class="media_link"></a>
	<?php
	if(!empty($phthumb)){
		// echo remove_width_attribute($phthumb);
		printf('<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="%s" class="b-lazy">',
			$phthumb[0]
		);
	} else {
	?>
	<img src="<?=$options['tpldir']?>/assets/img/preimg.jpg" alt="preimg">
	<?php } ?>
	<p class="media_date"><?php if(!empty($phmeta[$prefix.'gallery_date'][0])) echo strtr(date('d M', $phmeta[$prefix.'gallery_date'][0]), $options['translate']);?></p>
	<p class="media_name"><?=$post->post_title?></p>
	<p class="media_count"><span><?=count($images)?></span> фото</p>
</div>
<?php wp_reset_postdata(); ?>
