<?php
	global $options;
	$sid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$smeta=get_post_meta($sid, '', false);
	$imgid=(!empty($smeta[$prefix.'loopimg_id'][0])) ? $smeta[$prefix.'loopimg_id'][0] : null;
	if(!empty($smeta[$prefix.'block_col'][0])&&$smeta[$prefix.'block_transp'][0]<1){
		$hex = $smeta[$prefix.'block_col'][0];
		list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		// echo "$hex -> $r $g $b";
		$bgc='style="background:rgba('.$r.','.$g.','.$b.','.$smeta[$prefix.'block_transp'][0].')"';
		// $backgr='fill:rgba('.$r.','.$g.','.$b.','.$smeta[$prefix.'block_transp'][0].')';
	} elseif(!empty($smeta[$prefix.'block_col'][0])&&$smeta[$prefix.'block_transp'][0]==1){
		$transp='0.74';
		$hex = $smeta[$prefix.'block_col'][0];
		list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		// echo "$hex -> $r $g $b";
		$bgc='style="background:rgba('.$r.','.$g.','.$b.','.$transp.')"';
		// $backgr='fill:rgba('.$r.','.$g.','.$b.','.$transp.')"';
	}
	?>
	<div class="ell">
		<a href="<?=get_the_permalink()?>"></a>
		<img class="b-lazy" src="<?=$options['tpldir']?>/assets/img/loader.gif" data-src="<?=wp_get_attachment_image_src( $imgid, 'shift-mainimg-loop')[0]?>" alt="<?=$ttl?>" title="<?=$ttl?>">
		<ul class="shape" <?=$bgc?>>
			<li>
				<?php if(!empty($smeta[$prefix.'icon'][0])){
					if (strpos( $smeta[$prefix.'icon'][0], '.svg' )>0) {
						echo '<div class="iconsvg">'.file_get_contents(get_attached_file( $smeta[$prefix.'icon_id'][0] )).'</div>';
					} else {
						printf('<img class="b-lazy" src="%s" data-src="%s" alt="%s">',
							$options['tpldir'].'/assets/img/loader.gif',
							$smeta[$prefix.'icon'][0],
							$ttl
						);
					}
				} elseif(!empty($smeta[$prefix.'icon_sel'][0])&&$smeta[$prefix.'icon_sel'][0]!=='') {
					echo '<i class="'.$smeta[$prefix.'icon_sel'][0].'"></i>';
				} else {
					echo '<i class="icon-cam"></i>';
				} ?>
			</li>
			<li class="h1">
				<p> <?php echo (empty($smeta[$prefix.'loopttl'][0])) ? $ttl : $smeta[$prefix.'loopttl'][0]; ?> </p>
			</li>
			<li class="text">
				<p>	<?php $list=maybe_unserialize($smeta[$prefix.'loopdiv'][0]); if(!empty($list)) {foreach ( $list as $key => $val) {
					echo $val.'<br>';
				} } ?> </p>
			</li>
			<li class="date">
				<?php
				$dcol=(!empty($smeta[$prefix.'block_col'][0])) ? $smeta[$prefix.'block_col'][0] : '#000';
				if(!empty($smeta[$prefix.'tag_text_show'][0])&&!empty($smeta[$prefix.'tag_text'][0])){
					echo '<div class="allrent" style="color:'.$dcol.'">'.$smeta[$prefix.'tag_text'][0].'</div>';
				} else {
					if(empty($smeta[$prefix.'outofstock'][0])&&date('U', time())<$smeta[$prefix.'shift_date_out'][0]){
						echo strtr(date('d M', $smeta[$prefix.'shift_date_in'][0]), $options['translate']).' — '.strtr(date('d M', $smeta[$prefix.'shift_date_out'][0]), $options['translate']);
					} elseif(!empty($smeta[$prefix.'outofstock'][0])) {
						echo '<div class="allrent" style="color:'.$dcol.'">'.((!empty($smeta[$prefix.'tag_text'][0])) ? $smeta[$prefix.'tag_text'][0] : 'Мест нет').'</div>';
					}
					if(date('U', time())>$smeta[$prefix.'shift_date_out'][0]) {
						echo '<div class="allrent" style="color:'.$dcol.'">'.((!empty($smeta[$prefix.'tag_text'][0])) ? $smeta[$prefix.'tag_text'][0] : 'Смена закончилась').'</div>';
					}
				} ?>
			</li>
		</ul>
	</div>
