<?php get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	if(!empty($pmeta[$prefix.'block_col'][0])&&$pmeta[$prefix.'block_transp'][0]<1){
		$hex = $pmeta[$prefix.'block_col'][0];
		list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
		// echo "$hex -> $r $g $b";
		$bgc='style="background:rgba('.$r.','.$g.','.$b.','.$pmeta[$prefix.'block_transp'][0].')"';
		$backgr='fill:rgba('.$r.','.$g.','.$b.','.$pmeta[$prefix.'block_transp'][0].')';
	} elseif(!empty($pmeta[$prefix.'block_col'][0])&&$pmeta[$prefix.'block_transp'][0]==1){
		$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
		$backgr='fill:rgba('.$r.','.$g.','.$b.',1)"';
	}
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<section id="shiftslick" class="js-pad-top">
			<div class="shift">
				<div class="slide">
					<div class="img-cont">
						<img src="<?=$pthumb?>">
					</div>
					<div class="container abs">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-lg-6 col-lg-offset-1<?php echo ($pmeta[$prefix.'topblock_pos'][0]=='right') ? ' pull-right' : null; ?>">
								<?php if(!empty($pmeta[$prefix.'icon_sel'][0])&&$pmeta[$prefix.'icon_sel'][0]!==''){
									echo '<i class="'.$pmeta[$prefix.'icon_sel'][0].' shift_icon" '.$tcol.'></i>';
								} elseif(!empty($pmeta[$prefix.'icon'][0])) {
									if (strpos( $pmeta[$prefix.'icon'][0], '.svg' )>0) {
										echo '<div class="iconsvg">'.file_get_contents(get_attached_file( $pmeta[$prefix.'icon_id'][0] )).'</div>';
									} else {
										printf('<img class="b-lazy" src="%s" data-src="%s" alt="%s">',
											$options['tpldir'].'/assets/img/loader.gif',
											$pmeta[$prefix.'icon'][0],
											$ttl
										);
									}
								} else {
									echo '<i class="icon-cam shift_icon" '.$tcol.'></i>';
								} ?>
								<h1 class="shift_h1" <?=$tcol?>><?=$ttl?></h1>
								<p class="shift_date" <?=$tcol?>><?=strtr(date('d M', $pmeta[$prefix.'shift_date_in'][0]), $options['translate']).' — '.strtr(date('d M', $pmeta[$prefix.'shift_date_out'][0]), $options['translate'])?></p>
								<style>.iconsvg svg{width:70px;height:70px}.iconsvg svg path{fill:<?=$pmeta[$prefix.'txt_col'][0]?>}</style>
							</div>
							<div class="col-xs-12 col-sm-8 col-lg-7<?= ($pmeta[$prefix.'topblock_pos'][0]=='right') ? ' pull-right' : null; ?>">
								<div class="shift_shape" <?=$bgc?>>
									<?php the_content(); ?>
									<p class="shift_price">
										<?=number_format($pmeta[$prefix.'price'][0], 0, '', ' ')?> <span>грн.</span>
									</p>
									<div class="bott">
										<div class="bot-cont ls">
											<p class="place">
												<span class="digit"><?=$pmeta[$prefix.'left_places'][0]?></span>
												<span class="letter">мест <br> осталось</span>
											</p>
										</div>
										<div class="bot-cont">
											<a href="#" target="_blank" class="btn btn-purple popup-btn">Забронировать место</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="clist">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<p class="h1">Эта смена для тех</p>
						<ul class="checklist">
							<?php
							$list=maybe_unserialize($pmeta[$prefix.'for_those'][0]);
							if(!empty($list)){
							foreach ($list as $k => $v) { ?>
							<li><p><?=$v?></p></li>
							<?php } } ?>
						</ul>
					</div>
					<div class="col-xs-12 col-md-6">
						<p class="h1">Что дает смена</p>
						<ul class="checklist">
							<?php
							$list=maybe_unserialize($pmeta[$prefix.'what_gives'][0]);
							if(!empty($list)){
							foreach ($list as $k => $v) { ?>
							<li><p><?=$v?></p></li>
							<?php } } ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="grey">
			<div class="container ">
				<div class="row">
					<?php
					$list=maybe_unserialize($pmeta[$prefix.'courses'][0]);
					if(!empty($list)){
						foreach ($list as $l) { ?>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<p class="name"><?=$l['title']?>
							<!-- <span>ПРОФКУРС</span> -->
						</p>
						<div><?=$l['description']?></div>
					</div>
						<?php }
					}
					?>
				</div>
			</div>
		</section>
		<?php get_template_part( 'assets/php/blocks/block', 'schedule' ); ?>
		<?php get_template_part( 'assets/php/blocks/block', 'trainers' ); ?>
		<?php get_template_part( 'assets/php/blocks/block', 'review' ); ?>
		<?php get_template_part( 'assets/php/blocks/block', 'photo' ); ?>
		<?php get_template_part( 'assets/php/blocks/block', 'video' ); ?>
		<section id="abb">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9 col-md-offset-2">
						<?php if(!empty($pmeta[$prefix.'icon'][0])){
							printf('<img class="b-lazy" src="%s" data-src="%s" alt="%s" style="min-width:70px">',
								$options['tpldir'].'/assets/img/loader.gif',
								$pmeta[$prefix.'icon'][0],
								$ttl
							);
						} else {
							echo '<i class="icon-cam shift_icon"></i>';
						} ?>
					</div>
					<div class="col-xs-12 col-md-9 col-md-offset-2">
						<p class="half h1"><?=$ttl?></p>
						<p class="half h1 text-center"><?=number_format($pmeta[$prefix.'price'][0], 0, '', ' ')?> <span>грн.</span></p>
					</div>
					<div class="col-xs-12 col-md-9 col-md-offset-2">
						<div class="half half-bott">
							<div class="bott">
								<div class="bot-cont ls">
									<p class="place">
										<span class="digit"><?=$pmeta[$prefix.'left_places'][0]?></span>
										<span class="letter">мест <br> осталось</span>
									</p>
								</div>
							</div>
							<p class="date"><?=strtr(date('d M', $pmeta[$prefix.'shift_date_in'][0]), $options['translate']).' — '.strtr(date('d M', $pmeta[$prefix.'shift_date_out'][0]), $options['translate'])?></p>
						</div>
						<div class="half half-bott text-center">
							<div class="btn btn-purple popup-btn">Забронировать место</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php get_template_part( 'assets/php/blocks/block', 'docs' ); ?>
		<section id="map"></section>
	<?php endwhile; ?>

	<?php else: ?>
		<section>
			<article>
				<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
			</article>
		</section>
	<?php endif; ?>
	</main>

<?php get_footer(); ?>
