<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'startimecamp_';

/**
 * Initiate the metabox
 */
if(isset($_GET['post'])){
	$post_id = $_GET['post'];
} elseif(isset($_POST['post_ID'])){
	$post_id = $_POST['post_ID'];
} else {
	$post_id='';
}
// print_r($post_id);
//$post_id = get_the_ID() ;
$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
// $pt=get_post_type($post_id);
// $pt=get_current_screen()->post_type;
// check for a template type
if ($template_file == 'template-main.php') {
	$cmb = new_cmb2_box( array(
		'id'			=> 'global_metabox',
		'title'		 => __( 'Данные для этой страницы', 'cmb2' ),
		'object_types'  => array( 'page', ), // Post type
		'context'	   => 'normal',
		'priority'	  => 'high',
		'show_names'	=> true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'	 => true, // Keep the metabox closed by default
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Заголовок для данной страницы', 'cmb2' ),
		'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
		'id'		 => $prefix . 'title',
		'type'	   => 'text',
		'sanitization_cb' => 'unfilter_html_tags'
	) );
	$advantages_group = $cmb->add_field( array(
		'id'		  => $prefix.'advantages',
		'type'		=> 'group',
		'description' => __( 'Преимущества', 'startimecamp' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'	 => array(
			'group_title'   => __( 'Элемент {#}', 'startimecamp' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'	=> __( 'Добавить еще элемент', 'startimecamp' ),
			'remove_button' => __( 'Удалить элемент', 'startimecamp' ),
			'sortable'	  => true, // beta
			// 'closed'	 => true, // true to have the groups closed by default
		),
	) );
	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $advantages_group, array(
		'name' => __( 'Название преимущества', 'startimecamp' ),
		'desc' => __( 'можно задать html со знаками переноса', 'cmb2' ),
		'id'   => 'title',
		'type' => 'text',
		'sanitization_cb' => 'unfilter_html_tags',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );
	$cmb->add_group_field( $advantages_group, array(
		'name'	   => __( 'Выбор предустановленной иконки', 'cmb2' ),
		// 'desc'	   => __( 'выводится на главной и на странице смены', 'cmb2' ),
		'id'		 => 'icon',
		'type'	   => 'select',
		'show_option_none' => true,
		// 'default'		  => 'left',
		'options'		  => array(
			'icon-tree' => __( 'Дерево', 'cmb2' ),
			'icon-t-shorh'   => __( 'Футболка', 'cmb2' ),
			'icon-rew'   => __( 'Кубок', 'cmb2' ),
			'icon-scene'   => __( 'Сцена', 'cmb2' ),
			'icon-light'   => __( 'Лампочка', 'cmb2' ),
		),
	) );
	$cmb->add_group_field( $advantages_group, array(
		'name' => __( 'Изображение', 'startimecamp' ),
		'desc' => __( 'задать свою иконку - проигнорируется иконка из списка выше', 'cmb2' ),
		'id'   => 'icon-img',
		'type' => 'file',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
		// 'sanitization_cb' => 'unfilter_html_tags',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Описание преимуществ', 'cmb2' ),
		'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
		'id'		 => $prefix . 'adv-descr',
		'type'	   => 'wysiwyg',
		// 'sanitization_cb' => 'unfilter_html_tags'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Видео', 'cmb2' ),
		'desc'	   => __( 'Блок кроссбраузерного видео. Для широкой поддержки нужно как минимум 2 файла видео - mp4 и webm. Если видео имеет размер больше 100мб и загружается для отображения на сайте - во Вселенной погибает цивилизация.<br>Если задан id видео youtube - будет загружаться видео youtube, независимо от загрузки видео в другие поля', 'cmb2' ),
		'id'		 => $prefix . 'videos_title',
		'type'	   => 'title'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Постер для видео', 'cmb2' ),
		'id'		 => $prefix . 'video_poster',
		'type'	   => 'file',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
		// 'query_type' => 'image/jpeg, image/gif, image/png, image/bmp, image/tiff'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Видео mp4', 'cmb2' ),
		'id'		 => $prefix . 'videos_mp4',
		'type'	   => 'file'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Видео webm', 'cmb2' ),
		'id'		 => $prefix . 'videos_webm',
		'type'	   => 'file'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Youtube видео', 'cmb2' ),
		'desc'	=> 'сюда вставляем <b>ТОЛЬКО ID ВИДЕО</b>',
		'id'		 => $prefix . 'yt_id',
		'type'	   => 'text'
	) );
	for ($i=0; $i <= 4; $i++) {
		$im=$i+1;
		$cmb->add_field( array(
			'name'	   => __( 'Текст для блока питания '.$im, 'cmb2' ),
			'id'		 => $prefix . 'food'.$i,
			'type'	   => 'textarea_small',
			'sanitization_cb' => 'unfilter_html_tags'
		) );
	}
	$cmb->add_field( array(
		'name'	   => __( 'Меню', 'cmb2' ),
		'id'		 => $prefix . 'menu_title',
		'type'	   => 'title'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Файл меню', 'cmb2' ),
		'id'		 => $prefix . 'file_menu',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Проживание', 'cmb2' ),
		// 'desc'	   => __( 'Блок кроссбраузерного видео. Для широкой поддержки нужно как минимум 2 файла видео - mp4 и webm. Если видео имеет размер больше 50мб и загружается для отображения на сайте - во Вселенной погибает цивилизация.', 'cmb2' ),
		'id'		 => $prefix . 'living_title',
		'type'	   => 'title'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Изображение для блока "проживание"', 'cmb2' ),
		'id'		 => $prefix . 'living_img',
		'type'	   => 'file'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Текст для блока "проживание"', 'cmb2' ),
		'id'		 => $prefix . 'living_text',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Каждый номер оборудован:', 'cmb2' ),
		// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
		'id'		 => $prefix . 'every-room',
		'type'	   => 'text',
		'repeatable' => true
	) );
	$cmb->add_field( array(
		'name'	   => __( 'А также:', 'cmb2' ),
		// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
		'id'		 => $prefix . 'every-room-also',
		'type'	   => 'text',
		'repeatable' => true
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Родителям на заметку:', 'cmb2' ),
		// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
		'id'		 => $prefix . 'every-room-parents',
		'type'	   => 'text',
		'repeatable' => true
	) );
	$cmb->add_field( array(
		'name'	=> __( 'Прикрепленные Отзывы', 'cmb2' ),
		'desc'	=> __( 'Перенесите мышкой записи из левой колонки в правую. Также можно изменять порядок записей.', 'cmb2' ),
		'id'	  => $prefix.'attached_teacher',
		'type'	=> 'custom_attached_posts',
		'options' => array(
			'show_thumbnails' => false, // Show thumbnails on the left
			'filter_boxes'	=> false, // Show a text box for filtering the results
			'query_args'	  => array(
				'posts_per_page' => 50,
				'post_type'	  => 'teacher',
			), // override the get_posts args
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Карта трансфера', 'cmb2' ),
		'id'		 => $prefix . 'transfer_map',
		'type'	   => 'file',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Текст для блока "трансфер"', 'cmb2' ),
		'id'		 => $prefix . 'transfer_text',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Файл для блока "трансфер"', 'cmb2' ),
		'id'		 => $prefix . 'transfer_file',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf'
		)
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Подпись под файлом для блока "трансфер"', 'cmb2' ),
		'id'		 => $prefix . 'transfer_file_descr',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Прайс', 'cmb2' ),
		'id'		 => $prefix . 'files_price',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Анкета', 'cmb2' ),
		'id'		 => $prefix . 'files_table',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Информация для родителей', 'cmb2' ),
		'id'		 => $prefix . 'files_info',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Правила лагеря', 'cmb2' ),
		'id'		 => $prefix . 'files_rules',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Что взять с собой', 'cmb2' ),
		'id'		 => $prefix . 'files_wares',
		'type'	   => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Галерея', 'cmb2' ),
		'id'		 => $prefix . 'gallery',
		'type'	   => 'file_list',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
	) );
	$cmb->add_field( array(
		'name'	=> __( 'Прикрепленные Отзывы', 'cmb2' ),
		'desc'	=> __( 'Перенесите мышкой записи из левой колонки в правую. Также можно изменять порядок записей.', 'cmb2' ),
		'id'	  => $prefix.'attached_review',
		'type'	=> 'custom_attached_posts',
		'options' => array(
			'show_thumbnails' => false, // Show thumbnails on the left
			'filter_boxes'	=> false, // Show a text box for filtering the results
			'query_args'	  => array(
				'posts_per_page' => 50,
				'post_type'	  => 'review',
			), // override the get_posts args
		),
	) );
}
if ($template_file == 'template-about.php') {
	$cmb = new_cmb2_box( array(
		'id'			=> 'about_metabox',
		'title'		 => __( 'Данные для этой страницы', 'cmb2' ),
		'object_types'  => array( 'page', ), // Post type
		'context'	   => 'normal',
		'priority'	  => 'high',
		'show_names'	=> true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'	 => true, // Keep the metabox closed by default
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Заголовок для данной страницы', 'cmb2' ),
		// 'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
		'id'		 => $prefix . 'title',
		'type'	   => 'text',
		'sanitization_cb' => 'unfilter_html_tags'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Заголовок для раздела "условия"', 'cmb2' ),
		// 'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
		'id'		 => $prefix . 'condtitle',
		'type'	   => 'text',
		'sanitization_cb' => 'unfilter_html_tags'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Условия проживания', 'cmb2' ),
		'id'		 => $prefix . 'conditions',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Слайдер 1', 'cmb2' ),
		'id'		 => $prefix.'slider1',
		'type'	   => 'file_list'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Инфраструктура', 'cmb2' ),
		'id'		 => $prefix . 'infrastructure',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Слайдер 2', 'cmb2' ),
		'id'		 => $prefix.'slider2',
		'type'	   => 'file_list'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Дополнительные развлечения', 'cmb2' ),
		'id'		 => $prefix . 'add_entertainment',
		'type'	   => 'wysiwyg'
	) );
	$cmb->add_field( array(
		'name'	   => __( 'Об организаторе', 'cmb2' ),
		'id'		 => $prefix . 'about_org',
		'type'	   => 'wysiwyg'
	) );
}
// teacher data
$cmb_teacherdata = new_cmb2_box( array(
	'id'			=> 'teacher_metabox',
	'title'		 => __( 'Информация о преподавателе', 'cmb2' ),
	'object_types'  => array( 'teacher' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_teacherdata->add_field( array(
	'name'	   => __( 'Статус', 'cmb2' ),
	// 'desc'	   => __( 'Задайте описание для вкладки "описание"', 'cmb2' ),
	'id'		 => $prefix . 'position',
	'type'	   => 'text'
) );
$cmb_teacherdata->add_field( array(
	'name'	   => __( 'Награды и достижения', 'cmb2' ),
	// 'desc'	   => __( 'Задайте описание для вкладки "описание"', 'cmb2' ),
	'id'		 => $prefix . 'awards',
	'type'	   => 'textarea_small'
) );
// video data
$cmb_videodata = new_cmb2_box( array(
	'id'			=> 'video_metabox',
	'title'		 => __( 'Информация о видео', 'cmb2' ),
	'object_types'  => array( 'video' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_videodata->add_field( array(
	'name'	   => __( 'ID видео на Youtube', 'cmb2' ),
	'desc'	   => __( 'сюда вставляем только id видео', 'cmb2' ),
	'id'		 => $prefix . 'videoid',
	'type'	   => 'text'
) );
// artist data
$cmb_artistdata = new_cmb2_box( array(
	'id'			=> 'artist_metabox',
	'title'		 => __( 'Информация об артисте', 'cmb2' ),
	'object_types'  => array( 'artist' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_artistdata->add_field( array(
	'name'	   => __( 'Фамилия/наименование', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем только id видео', 'cmb2' ),
	'id'		 => $prefix . 'title',
	'type'	   => 'text'
) );
// team data
$cmb_teamdata = new_cmb2_box( array(
	'id'			=> 'team_metabox',
	'title'		 => __( 'Информация о члене команды', 'cmb2' ),
	'object_types'  => array( 'team' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_teamdata->add_field( array(
	'name'	   => __( 'Положение', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем только id видео', 'cmb2' ),
	'id'		 => $prefix . 'position',
	'type'	   => 'text'
) );
$cmb_teamdata->add_field( array(
	'name'	   => __( 'Телефон', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем только id видео', 'cmb2' ),
	'id'		 => $prefix . 'phone',
	'type'	   => 'text'
) );
$cmb_teamdata->add_field( array(
	'name'	   => __( 'Email', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем только id видео', 'cmb2' ),
	'id'		 => $prefix . 'email',
	'type'	   => 'text'
) );

// shift meta
$cmb_shiftdata = new_cmb2_box( array(
	'id'			=> 'shift_metabox',
	'title'		 => __( 'Информация о смене', 'cmb2' ),
	'object_types'  => array( 'shift' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Изображение для архива', 'cmb2' ),
	'desc'	   => __( 'кратно 376х435', 'cmb2' ),
	'id'		 => $prefix . 'loopimg',
	'type'	   => 'file',
	'query_args' => array(
		'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
	)
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Заголовок для архива', 'cmb2' ),
	'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки со знаками прерывания строки в нужном месте', 'cmb2' ),
	'id'		 => $prefix . 'loopttl',
	'type'	   => 'text',
	'sanitization_cb' => 'unfilter_html_tags'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Направления для архива', 'cmb2' ),
	// 'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
	'id'		 => $prefix . 'loopdiv',
	'type'	   => 'text',
	'repeatable' => true
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Текст описания для третьего экрана на главной', 'cmb2' ),
	// 'desc'	   => __( 'Мы не сможем сделать вывод символа новой строки для каждого заголовка индивидуально в нужном месте, поэтому, если нужно выводить заголовок в архиве особым образом - нужно тут указать чистый html без обертки', 'cmb2' ),
	'id'		 => $prefix . 'maintext',
	'type'	   => 'wysiwyg'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'С какой стороны информация на главном экране', 'cmb2' ),
	// 'desc'	   => __( 'выводится на главной и на странице смены', 'cmb2' ),
	'id'		 => $prefix . 'topblock_pos',
	'type'	   => 'select',
	'show_option_none' => false,
	'default'		  => 'left',
	'options'		  => array(
		'left' => __( 'Слева', 'cmb2' ),
		'right'   => __( 'Справа', 'cmb2' )
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Иконка', 'cmb2' ),
	'desc'	   => __( 'выводится на главной и на странице смены', 'cmb2' ),
	'id'		 => $prefix . 'icon_sel',
	'type'	   => 'select',
	'show_option_none' => true,
	// 'default'		  => 'icon-cam',
	'options'		  => array(
		// 'none' => __( 'Нет', 'cmb2' ),
		'icon-cam' => __( 'Камера', 'cmb2' ),
		'icon-boll'   => __( 'Мяч', 'cmb2' ),
		'icon-speech'   => __( 'Речь', 'cmb2' ),
		'icon-mask'   => __( 'Маски', 'cmb2' ),
		'icon-ic1'   => __( 'Пропуск', 'cmb2' ),
		'icon-sup'   => __( 'Лидер', 'cmb2' ),
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Иконка', 'cmb2' ),
	'desc'	   => __( 'если не задана иконка в верхнем выпадающем меню', 'cmb2' ),
	'id'		 => $prefix . 'icon',
	'type'	   => 'file',
	'query_args' => array(
		'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
	)
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Дата заезда', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'shift_date_in',
	'type'	   => 'text_date_timestamp',
	// 'date_format' => 'd M, D'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Дата выезда', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'shift_date_out',
	'type'	   => 'text_date_timestamp',
	// 'date_format' => 'd M, D'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Отображать кастомный ярлык на странице архива?', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'tag_text_show',
	'type'	   => 'checkbox',
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Текст для ярлыка на странице архива', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'tag_text',
	'type'	   => 'text',
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Цена', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем описание победителя в конкурсе', 'cmb2' ),
	'id'		 => $prefix . 'price',
	'type'	   => 'text',
	'attributes' => array(
		'type' => 'number',
		'pattern' => '\d*',
		'step' => '1',
		'min' => '0'
	),
	'sanitization_cb' => 'float',
	'escape_cb'	   => 'float'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Мест осталось', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем описание победителя в конкурсе', 'cmb2' ),
	'id'		 => $prefix . 'left_places',
	'type'	   => 'text',
	'attributes' => array(
		'type' => 'number',
		'pattern' => '\d*',
		'step' => '1',
		'min' => '0'
	),
	'sanitization_cb' => 'float',
	'escape_cb'	   => 'float'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Мест нет', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем описание победителя в конкурсе', 'cmb2' ),
	'id'		 => $prefix . 'outofstock',
	'type'	   => 'checkbox'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Шорткод формы', 'cmb2' ),
	// 'desc'	   => __( 'сюда вставляем описание победителя в конкурсе', 'cmb2' ),
	'id'		 => $prefix . 'form_sc',
	'type'	   => 'text',
) );
$cmb_shiftdata->add_field( array(
	'name'	=> 'Выбор цвета для текста на первом экране',
	'id'	  => $prefix . 'txt_col',
	'type'	=> 'colorpicker',
	'default' => '#ffffff'
) );
$cmb_shiftdata->add_field( array(
	'name'	=> 'Выбор цвета для фона блока на первом экране',
	'id'	  => $prefix . 'block_col',
	'type'	=> 'colorpicker',
	'default' => '#ffffff'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Прозрачность', 'cmb2' ),
	'desc'	   => __( '0 - невидимый, 1 - непрозрачный', 'cmb2' ),
	'id'		 => $prefix . 'block_transp',
	'type'	   => 'text',
	'default'	 => 1,
	'attributes' => array(
		'type' => 'number',
		'pattern' => '\d*',
		'step' => '.01',
		'min' => '0'
	),
	'sanitization_cb' => 'float',
	'escape_cb'	   => 'float'
) );
$cmb_shiftdata->add_field( array(
	'name' => __( 'Эта смена для тех...', 'startimecamp' ),
	'id'   => $prefix . 'for_those',
	'type' => 'text',
	'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$cmb_shiftdata->add_field( array(
	'name' => __( 'Что дает...', 'startimecamp' ),
	'id'   => $prefix . 'what_gives',
	'type' => 'text',
	'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$courses_group = $cmb_shiftdata->add_field( array(
	'id'		  => $prefix.'courses',
	'type'		=> 'group',
	// 'description' => __( 'Профкурсы', 'startimecamp' ),
	// 'repeatable'  => false, // use false if you want non-repeatable group
	'options'	 => array(
		'group_title'   => __( 'Элемент {#}', 'startimecamp' ), // since version 1.1.4, {#} gets replaced by row number
		'add_button'	=> __( 'Добавить еще элемент', 'startimecamp' ),
		'remove_button' => __( 'Удалить элемент', 'startimecamp' ),
		'sortable'	  => true, // beta
		// 'closed'	 => true, // true to have the groups closed by default
	),
) );
// Id's for group's fields only need to be unique for the group. Prefix is not needed.
$cmb_shiftdata->add_group_field( $courses_group, array(
	'name' => __( 'Название курса', 'startimecamp' ),
	'id'   => 'title',
	'type' => 'text',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$cmb_shiftdata->add_group_field( $courses_group, array(
	'name' => __( 'Описание курса', 'startimecamp' ),
	'id'   => 'description',
	'type' => 'wysiwyg',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$courses_group = $cmb_shiftdata->add_field( array(
	'id'		  => $prefix.'schedule',
	'type'		=> 'group',
	'description' => __( 'Расписание дня', 'startimecamp' ),
	// 'repeatable'  => false, // use false if you want non-repeatable group
	'options'	 => array(
		'group_title'   => __( 'Элемент {#}', 'startimecamp' ), // since version 1.1.4, {#} gets replaced by row number
		'add_button'	=> __( 'Добавить еще элемент', 'startimecamp' ),
		'remove_button' => __( 'Удалить элемент', 'startimecamp' ),
		'sortable'	  => true, // beta
		// 'closed'	 => true, // true to have the groups closed by default
	),
) );
$cmb_shiftdata->add_group_field( $courses_group, array(
	'name' => __( 'Описание элемента', 'startimecamp' ),
	'id'   => 'cont',
	'type' => 'textarea_small',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$cmb_shiftdata->add_group_field( $courses_group, array(
	'name' => __( 'Время начала', 'startimecamp' ),
	'id'   => 't_from',
	'type' => 'text_time',
	'time_format' => 'H:i',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$cmb_shiftdata->add_group_field( $courses_group, array(
	'name' => __( 'Время конца', 'startimecamp' ),
	'id'   => 't_to',
	'type' => 'text_time',
	'time_format' => 'H:i',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );
$cmb_shiftdata->add_field( array(
	'name'	=> __( 'Прикрепленные Отзывы', 'cmb2' ),
	'desc'	=> __( 'Перенесите мышкой записи из левой колонки в правую. Также можно изменять порядок записей.', 'cmb2' ),
	'id'	  => $prefix.'attached_review',
	'type'	=> 'custom_attached_posts',
	'options' => array(
		'show_thumbnails' => false, // Show thumbnails on the left
		'filter_boxes'	=> false, // Show a text box for filtering the results
		'query_args'	  => array(
			'posts_per_page' => 50,
			'post_type'	  => 'review',
		), // override the get_posts args
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	=> __( 'Прикрепленные Преподаватели', 'cmb2' ),
	'desc'	=> __( 'Перенесите мышкой записи из левой колонки в правую. Также можно изменять порядок записей.', 'cmb2' ),
	'id'	  => $prefix.'attached_teacher',
	'type'	=> 'custom_attached_posts',
	'options' => array(
		'show_thumbnails' => true, // Show thumbnails on the left
		'filter_boxes'	=> false, // Show a text box for filtering the results
		'query_args'	  => array(
			'posts_per_page' => 30,
			'post_type'	  => 'teacher',
		), // override the get_posts args
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Фото для галереи', 'cmb2' ),
	// 'desc'	   => __( 'оставить пустым чтобы использовать стандартную форму под названием "Форма для кастинга"', 'cmb2' ),
	'id'		 => $prefix . 'photos',
	'type'	   => 'file_list',
	'query_args' => array(
		'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
	)
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Видео', 'cmb2' ),
	'desc'	   => __( 'Блок кроссбраузерного видео. Для широкой поддержки нужно как минимум 2 файла видео - mp4 и webm. Если видео имеет размер больше 50мб и загружается для отображения на сайте - во Вселенной погибает цивилизация.', 'cmb2' ),
	'id'		 => $prefix . 'videos_title',
	'type'	   => 'title'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Постер для видео', 'cmb2' ),
	// 'desc'	   => __( 'Блок кроссбраузерного видео. Для широкой поддержки нужно как минимум 2 файла видео - mp4 и webm. Если видео имеет размер больше 50мб и загружается для отображения на сайте - во Вселенной погибает цивилизация.', 'cmb2' ),
	'id'		 => $prefix . 'video_poster',
	'type'	   => 'file',
	'query_args' => array(
		'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
	)
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Видео mp4', 'cmb2' ),
	'id'		 => $prefix . 'videos_mp4',
	'type'	   => 'file'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Видео webm', 'cmb2' ),
	'id'		 => $prefix . 'videos_webm',
	'type'	   => 'file'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Файлы для скачивания', 'cmb2' ),
	'id'		 => $prefix . 'files_title',
	'type'	   => 'title'
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Прайс', 'cmb2' ),
	'id'		 => $prefix . 'files_price',
	'type'	   => 'file',
	'query_args' => array(
		'type' => 'application/pdf', // Make library only display PDFs.
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Анкета', 'cmb2' ),
	'id'		 => $prefix . 'files_table',
	'type'	   => 'file',
	'query_args' => array(
		'type' => 'application/pdf', // Make library only display PDFs.
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Информация для родителей', 'cmb2' ),
	'id'		 => $prefix . 'files_info',
	'type'	   => 'file',
	'query_args' => array(
		'type' => 'application/pdf', // Make library only display PDFs.
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Правила лагеря', 'cmb2' ),
	'id'		 => $prefix . 'files_rules',
	'type'	   => 'file',
	'query_args' => array(
		'type' => 'application/pdf', // Make library only display PDFs.
	),
) );
$cmb_shiftdata->add_field( array(
	'name'	   => __( 'Что взять с собой', 'cmb2' ),
	'id'		 => $prefix . 'files_wares',
	'type'	   => 'file',
	'query_args' => array(
		'type' => 'application/pdf', // Make library only display PDFs.
	),
) );

// review data
$cmb_reviewdata = new_cmb2_box( array(
	'id'			=> 'review_metabox',
	'title'		 => __( 'Информация об отзыве', 'cmb2' ),
	'object_types'  => array( 'review' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_reviewdata->add_field( array(
	'name'	   => __( 'Позиция', 'cmb2' ),
	// 'desc'	   => __( 'выводится на главной и на странице смены', 'cmb2' ),
	'id'		 => $prefix . 'who',
	'type'	   => 'text'
) );
$cmb_reviewdata->add_field( array(
	'name'	   => __( 'Дата заезда', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'shift_date_in',
	'type'	   => 'text_date_timestamp',
	// 'date_format' => 'd M, D'
) );
$cmb_reviewdata->add_field( array(
	'name'	   => __( 'Дата выезда', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix . 'shift_date_out',
	'type'	   => 'text_date_timestamp',
	// 'date_format' => 'd M, D'
) );

// media meta
// TODO: finish all metaboxes
$cmb_mediadata = new_cmb2_box( array(
	'id'			=> 'media_metabox',
	'title'		 => __( 'Данные для медиа', 'cmb2' ),
	'object_types'  => array( 'media' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_mediadata->add_field( array(
	'name'	   => __( 'Тип записи', 'cmb2' ),
	// 'desc'	   => __( 'кратно 376х435', 'cmb2' ),
	'id'		 => $prefix.'post_type',
	'type'			 => 'select',
	'show_option_none' => false,
	'default'		  => 'photo',
	'options'		  => array(
		'photo' => __( 'Фото', 'cmb2' ),
		'video'   => __( 'Видео', 'cmb2' )
	),
) );
if(get_post_meta( $post_id, $prefix.'post_type', true ) == 'photo'){
	$cmb_mediadata->add_field( array(
		'name'	   => __( 'Изображения', 'cmb2' ),
		// 'desc'	   => __( 'кратно 376х435', 'cmb2' ),
		'id'		 => $prefix.'mediaimages',
		'type'	   => 'file_list',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
	) );
	$cmb_mediadata->add_field( array(
		'name'	   => __( 'Дата для этого элемента', 'cmb2' ),
		'id'		 => $prefix.'gallery_date',
		'type'	   => 'text_date_timestamp'
	) );
} elseif(get_post_meta( $post_id, $prefix.'post_type', true ) == 'video') {
	$cmb_mediadata->add_field( array(
		'name'	   => __( 'Изображение для постера видео', 'cmb2' ),
		// 'desc'	   => __( 'кратно 376х435', 'cmb2' ),
		'id'		 => $prefix.'posterimg',
		'type'	   => 'file',
		'query_args' => array(
			'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
		)
	) );
	// $cmb_mediadata->add_field( array(
	// 	'name'	   => __( 'Видео mp4', 'cmb2' ),
	// 	'id'		 => $prefix.'videos_mp4',
	// 	'type'	   => 'file'
	// ) );
	// $cmb_mediadata->add_field( array(
	// 	'name'	   => __( 'Видео webm', 'cmb2' ),
	// 	'id'		 => $prefix.'videos_webm',
	// 	'type'	   => 'file'
	// ) );
	$cmb_mediadata->add_field( array(
		'name'	   => __( 'Youtube видео', 'cmb2' ),
		'desc'	=> 'сюда вставляем <b>ТОЛЬКО ID ВИДЕО</b>',
		'id'		 => $prefix.'yt_id',
		'type'	   => 'text'
	) );
}


// $cmb_mediadata->add_field( array(
// 	'name'	   => __( 'Изображение для архива', 'cmb2' ),
// 	'desc'	   => __( 'кратно 376х435', 'cmb2' ),
// 	'id'		 => $prefix . 'loopimg',
// 	'type'	   => 'file',
// 	'query_args' => array(
// 		'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
// 	)
// ) );

// event data
$cmb_eventdata = new_cmb2_box( array(
	'id'			=> 'event_metabox',
	'title'		 => __( 'Информация о событии', 'cmb2' ),
	'object_types'  => array( 'event' ), // Post type
	'context'	   => 'normal',
	'priority'	  => 'high',
	'show_names'	=> true, // Show field names on the left
	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 'closed'	 => true, // Keep the metabox closed by default
) );
$cmb_eventdata->add_field( array(
	'name'	   => __( 'Изображение', 'cmb2' ),
	'desc'	   => __( 'большое изображение для отображения в модальном окне события', 'cmb2' ),
	'id'		 => $prefix . 'bigimg',
	'type'	   => 'file',
	// 'query_args' => array(
	// 	'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
	// )
) );
$cmb_eventdata->add_field( array(
	'name'	   => __( 'Форма', 'cmb2' ),
	'desc'	   => __( 'сюда вставить шорткод формы для этого события', 'cmb2' ),
	'id'		 => $prefix.'form',
	'type'	   => 'text'
) );
$cmb_eventdata->add_field( array(
	'name'	   => __( 'Дата и время события', 'cmb2' ),
	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
	'id'		 => $prefix.'time',
	'type'	   => 'text_datetime_timestamp',
	// 'date_format' => 'd M, D'
) );
// $cmb_eventdata->add_field( array(
// 	'name'	   => __( 'Время', 'cmb2' ),
// 	// 'desc'	   => __( 'Укажите адрес', 'cmb2' ),
// 	'id'		 => $prefix . 'shift_date_out',
// 	'type'	   => 'text_date_timestamp',
// 	// 'date_format' => 'd M, D'
// ) );
