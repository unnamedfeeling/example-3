<?php
register_post_type('teacher', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Преподаватели', 'startimecamp'), // Rename these to suit
		'singular_name' => __('Преподаватель', 'startimecamp'),
		'add_new' => __('Добавить', 'startimecamp'),
		'add_new_item' => __('Добавить преподавателя', 'startimecamp'),
		'edit' => __('Изменить', 'startimecamp'),
		'edit_item' => __('Изменить', 'startimecamp'),
		'new_item' => __('Новый преподаватель', 'startimecamp'),
		'view' => __('Просмотреть', 'startimecamp'),
		'view_item' => __('Просмотреть преподавателя', 'startimecamp'),
		'search_items' => __('Искать', 'startimecamp'),
		'not_found' => __('Не найдены преподаватели', 'startimecamp'),
		'not_found_in_trash' => __('Не найден ни один преподаватель в корзине', 'startimecamp')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		// 'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-welcome-learn-more',
	'menu_position'	   => 6,
	'publicly_queryable'  => false,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
register_post_type('shift', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Смены', 'startimecamp'), // Rename these to suit
		'singular_name' => __('Смена', 'startimecamp'),
		'add_new' => __('Добавить', 'startimecamp'),
		'add_new_item' => __('Добавить смену', 'startimecamp'),
		'edit' => __('Изменить', 'startimecamp'),
		'edit_item' => __('Изменить', 'startimecamp'),
		'new_item' => __('Новая смена', 'startimecamp'),
		'view' => __('Просмотреть', 'startimecamp'),
		'view_item' => __('Просмотреть', 'startimecamp'),
		'search_items' => __('Искать', 'startimecamp'),
		'not_found' => __('Не найдены смены', 'startimecamp'),
		'not_found_in_trash' => __('Не найден ни одна смена в корзине', 'startimecamp')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-universal-access',
	'menu_position'	   => 5,
	'publicly_queryable'  => true,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
register_post_type('event', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('События', 'startimecamp'), // Rename these to suit
		'singular_name' => __('Событие', 'startimecamp'),
		'add_new' => __('Добавить', 'startimecamp'),
		'add_new_item' => __('Добавить элемент', 'startimecamp'),
		'edit' => __('Изменить', 'startimecamp'),
		'edit_item' => __('Изменить элемент', 'startimecamp'),
		'new_item' => __('Новый элемент', 'startimecamp'),
		'view' => __('Просмотреть', 'startimecamp'),
		'view_item' => __('Просмотреть элемент', 'startimecamp'),
		'search_items' => __('Искать', 'startimecamp'),
		'not_found' => __('Не найдены элементы', 'startimecamp'),
		'not_found_in_trash' => __('Не найден ни один элемент в корзине', 'startimecamp')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => true,
	'supports' => array(
		'title',
		'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	// 'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-location',
	'menu_position'	   => 5,
	// 'publicly_queryable'  => false,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
// redirect single posts to the archive page, scrolled to current ID
// add_action( 'template_redirect', function() {
//	 if ( is_singular('event') ) {
//		 global $post;
//		 $redirectLink = get_post_type_archive_link( 'event' )."#".$post->ID;
//		 wp_redirect( $redirectLink, 302 );
//		 exit;
//	 }
// });
// turn off pagination for the archive page
// add_action('parse_query', function($query) {
//	 if (is_post_type_archive('event')) {
//		 $query->set('nopaging', 1);
// 		$query->set('posts_per_page', 6);
//	 }
// });
register_post_type('review', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Отзывы', 'startimecamp'), // Rename these to suit
		'singular_name' => __('Отзыв', 'startimecamp'),
		'add_new' => __('Добавить', 'startimecamp'),
		'add_new_item' => __('Добавить элемент', 'startimecamp'),
		'edit' => __('Изменить', 'startimecamp'),
		'edit_item' => __('Изменить элемент', 'startimecamp'),
		'new_item' => __('Новый элемент', 'startimecamp'),
		'view' => __('Просмотреть', 'startimecamp'),
		'view_item' => __('Просмотреть элемент', 'startimecamp'),
		'search_items' => __('Искать', 'startimecamp'),
		'not_found' => __('Не найдены элементы', 'startimecamp'),
		'not_found_in_trash' => __('Не найден ни один элемент в корзине', 'startimecamp')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => true,
	'supports' => array(
		'title',
		'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-exerpt-view',
	'menu_position'	   => 5,
	'publicly_queryable'  => true,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
register_post_type('media', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Медиа', 'startimecamp'), // Rename these to suit
		'singular_name' => __('Медиа', 'startimecamp'),
		'add_new' => __('Добавить', 'startimecamp'),
		'add_new_item' => __('Добавить элемент', 'startimecamp'),
		'edit' => __('Изменить', 'startimecamp'),
		'edit_item' => __('Изменить элемент', 'startimecamp'),
		'new_item' => __('Новый элемент', 'startimecamp'),
		'view' => __('Просмотреть', 'startimecamp'),
		'view_item' => __('Просмотреть элемент', 'startimecamp'),
		'search_items' => __('Искать', 'startimecamp'),
		'not_found' => __('Не найдены элементы', 'startimecamp'),
		'not_found_in_trash' => __('Не найден ни один элемент в корзине', 'startimecamp')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		// 'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-format-video',
	'menu_position'	   => 5,
	'publicly_queryable'  => true,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
