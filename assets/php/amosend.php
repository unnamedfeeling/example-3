<?php
global $options;
$submission = WPCF7_Submission::get_instance();
$postdata = $submission->get_posted_data();
$form = WPCF7_ContactForm::get_current();
$props = $form->get_properties();
// $id = $form->__get('id');
$id = $postdata['_wpcf7'];
$now=time();
$responce['curTime']=date('Y-m-d H:i:s');
$responce['curTimeNow']=date('Y-m-d H:i:s', $now);
$responce['now']='NOW';

require_once get_template_directory() . '/assets/php/amocrm-php/amocrm.phar';
try {
	// $amo = new \AmoCRM\Client('startimepc', 'sale@startime.ua', '671431e6bb815fe21ca8c5bf31eeee58');
	$amo = new \AmoCRM\Client('startimepc', 'sale@startime.ua', '12958f9a8e5bbc163915ddfe56bbdd8c');

	// $responce['amoStatus'] = $amo->account->apiCurrent();
	// $responce['amoAccount'] = $amo->account;

	$responce['foundContacts'] = $amo->contact->apiList([
		'query' => $postdata['your-phone'],
	]);

	// Добавление и обновление контактов
	// Метод позволяет добавлять контакты по одному или пакетно,
	// а также обновлять данные по уже существующим контактам.
	$contact = $amo->contact;
	//$contact->debug(true); // Режим отладки
	$contact['name'] = (!empty($postdata['your-name']))?$postdata['your-name']:'no-name-input';
	$contact['request_id'] = $postdata['_wpcf7'].'-'.$postdata['your-name'];
	$contact['date_create'] = $responce['now'];
	$contact['last_modified'] = $responce['now'];
	$contact['responsible_user_id'] = 898465;
	if (!empty($postdata['your-phone'])) {
		$contact->addCustomField(150621, [
			[$postdata['your-phone'], 'MOB'],
		]);
	}
	if (!empty($postdata['textarea-main'])) {
		$contact->addCustomField(156992, [
			[$postdata['textarea-main']],
		]);
	}
	if (!empty($postdata['birth_date'])) {
		$contact->addCustomField(156291, [
			[$postdata['birth_date']],
		]);
	}
	if (!empty($postdata['utm_source'])) {
		$contact->addCustomField(444077, [
			[$postdata['utm_source']],
		]);
	}
	if (!empty($postdata['utm_medium'])) {
		$contact->addCustomField(444075, [
			[$postdata['utm_medium']],
		]);
	}
	if (!empty($postdata['utm_campaign'])) {
		$contact->addCustomField(444079, [
			[$postdata['utm_campaign']],
		]);
	}
	if (!empty($postdata['utm_term'])) {
		$contact->addCustomField(444073, [
			[$postdata['utm_term']],
		]);
	}
	if (!empty($postdata['utm_ip'])) {
		$contact->addCustomField(444085, [
			[$postdata['utm_ip']],
		]);
	}
	if (!empty($postdata['utm_city'])) {
		$contact->addCustomField(444081, [
			[$postdata['utm_country'].'/'.$postdata['utm_city']],
		]);
		$contact->addCustomField(443189, [
			[$postdata['utm_city']],
		]);
	}
	if (!empty($postdata['utm_useragent'])) {
		$contact->addCustomField(444087, [
			[$postdata['utm_useragent']],
		]);
	}
	if (!empty($postdata['utm_page'])) {
		$contact->addCustomField(444089, [
			[$postdata['utm_page']],
		]);
	}
	if (!empty($postdata['utm_prevpage'])) {
		$contact->addCustomField(444083, [
			[$postdata['utm_prevpage']],
		]);
	}

	// add contact now
	if (empty($responce['foundContacts'])) {
		$responce['amoApiSentDataContact']=$contact;
		$id = $contact->apiAdd();
		$responce['amoDataContact']=$id;
	}


	$ind=11332219;

	// create new lead
	$lead = $amo->lead;
	// $lead->debug(true); // Режим отладки
	$lead['name'] = 'Новая сделка на сайте startimecamp.com';
	$lead['date_create'] = $responce['now'];
	$lead['status_id'] = $ind;
	// $lead['price'] = 3000;
	$lead['responsible_user_id'] = $contact['responsible_user_id'];
	$lead['tags'] = $tags;
	$responce['amoApiSentDataLead']=$lead;
	$leadid = $lead->apiAdd();
	$responce['amoDataLead']=$leadid;


	$link = $amo->links;
	$link['from'] = 'leads';
	$link['from_id'] = $leadid;
	$link['to'] = 'contacts';
	$link['to_id'] = (!empty($responce['amoDataContact']))?$responce['amoDataContact']:$responce['foundContacts'][0]['id'];
	$linkid=$link->apiLink();
	$responce['amoDataLink']=$linkid;

} catch (\AmoCRM\Exception $e) {
	$responce['apiError']=sprintf('Error (%d): %s', $e->getCode(), $e->getMessage());
}

// Set
$form->set_properties( $props );


// Debug
// $file='Data sent (POSTed): '.PHP_EOL."\n".json_encode($postdata).PHP_EOL."\n".'Responces:'.PHP_EOL."\n".json_encode($responce);
$file='Data sent (POSTed): '.PHP_EOL."\n".json_encode($postdata).PHP_EOL."\n".'Responces:'.PHP_EOL."\n".var_export($responce, true);
// $file='Data sent: '.PHP_EOL."\n".json_encode($postdata).PHP_EOL."\n".'$props:'.PHP_EOL."\n".json_encode($props).PHP_EOL."\n".'Data received:'.PHP_EOL."\n".json_encode($responce);
// $file='Data sent: '.PHP_EOL."\n".json_encode($opts).PHP_EOL."\n".'Data received:'.PHP_EOL."\n".var_export($responce ,true);
// $file='Data received:'.PHP_EOL."\n".var_export($responce ,true);
// $fe = fopen(ABSPATH . "test.txt", "w");
$date=date('d-m-Y--H-i-s', $now);
$fp = fopen(get_template_directory() . '/messages/'.$date.'.txt', 'w');
fwrite($fp, var_export($file ,true));
fclose($fp);
