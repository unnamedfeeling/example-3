<?php
global $pmeta, $prefix, $options;
if(!empty($pmeta[$prefix.'attached_teacher'][0])){
?>
<section id="block9">
	<div class="container">
		<h1>Команда</h1>
		<div class="team-loop">
			<?php
			$args=array(
				'post_type'=>'teacher',
				'posts_per_page'=>-1,
			);
			$teachersarr=maybe_unserialize($pmeta[$prefix.'attached_teacher'][0]);
			if(empty($teachersarr)){
				$args['orderby']='date';
				$args['order']='asc';
			} else {
				$args['post__in']=$teachersarr;
				$args['orderby']='post__in';
			}
			$teachers=get_posts($args);
			if(!empty($teachers)){
			foreach ($teachers as $t) {
				$tid=$t->ID;
				$tmeta=get_post_meta($tid, '', false);
				$tttl=$t->post_title;
				?>
				<div class="team-ell">
					<?php
					printf('<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="%s" alt="%s" class="photo b-lazy">',
						wp_get_attachment_image_url($tmeta['_thumbnail_id'][0], 'teacherimg'),
						$tttl
					);
					?>
					<p class="name"><?=$tttl?></p>
					<p class="ppos"><?=$tmeta[$prefix.'position'][0]?></p>
					<p><?=$tmeta[$prefix.'awards'][0]?></p>
				</div>
			<?php } } ?>
		</div>
	</div>
</section>
<?php } ?>
