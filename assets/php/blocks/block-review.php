<?php
global $pmeta, $prefix, $options;
$args=array(
	'post_type'=>'review',
	'posts_per_page'=>-1,
);
$reviewsarr=maybe_unserialize($pmeta[$prefix.'attached_review'][0]);
if(empty($reviewsarr)){
	$args['orderby']='date';
	$args['order']='asc';
} else {
	$args['post__in']=$reviewsarr;
	$args['orderby']='post__in';
}
$reviews=get_posts($args);
if(!empty($reviews)){ ?>
<section id="block12">
	<div class="container">
		<h1>отзывы</h1>
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="rev-loop">
					<?php
						foreach ($reviews as $r) {
							$rid=$r->ID;
							$rmeta=get_post_meta($rid, '', false);
							$rttl=$r->post_title;
							?>
							<div class="rev-ell">
								<?php
								printf('<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="%s" alt="%s" class="avat">',
									wp_get_attachment_image_url($rmeta['_thumbnail_id'][0], 'teacherimg'),
									$rttl
								);
								?>
								<div class="text">
									<div class="top">
										<p class="name"><?=$rttl?></p>
										<p class="cours"><?=$rmeta[$prefix.'who'][0]?></p>
										<p class="date"><?=strtr(date('d M', $rmeta[$prefix.'shift_date_in'][0]), $options['translate']).' — '.strtr(date('d M Y', $rmeta[$prefix.'shift_date_out'][0]), $options['translate'])?>
										</p>
									</div>
									<div class="trev">
										<?=$r->post_content?>
									</div>
								</div>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
