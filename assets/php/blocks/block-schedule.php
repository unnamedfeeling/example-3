<?php
global $pmeta, $prefix, $options;
if(!empty($pmeta[$prefix.'schedule'][0])){
?>
<section id="block8">
	<div class="container">
		<h1>
			Расписание дня<span>*</span>
		</h1>
		<div class="row mtable">
			<?php
			$list=maybe_unserialize($pmeta[$prefix.'schedule'][0]);
			$len = count($list);
			// $list=array_combine(range(1, $len), array_values($list));
			$c=$len%3;
			$div2=$len;
			switch ($c) {
				case 0:
					$div=$div1=$len / 3;
					break;
				case 1:
					// $div=$div1=$len / 3;
					$div=ceil($len / 3);
					$div1=floor($len / 3);
					break;
				case 2:
					$div=ceil($len / 3);
					$div1=ceil($len / 3);
					break;
			}
			// $div=($len%3==2) ? $len / 3 : ceil($len / 3);
			// $div1=($len%3==2) ? ceil($len / 3) : $len / 3;
			$list1=array_slice($list, 0, $div, true);
			$len_l1=count($list1);
			$list2=array_slice($list, $len_l1, $div1, true);
			$len_l2=$len_l1+count($list2);
			$list3=array_slice($list, $len_l2, $div2, true);
			?>
			<div class="col-xs-12 col-sm-4">
				<?php foreach ($list1 as $el) { ?>
				<div class="ell">
					<div class="date">
						<p><?=$el['t_from'].'<br>'.$el['t_to']?></p>
					</div>
					<div class="discr">
						<p><?=$el['cont']?></p>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php foreach ($list2 as $el) { ?>
				<div class="ell">
					<div class="date">
						<p><?=$el['t_from'].'<br>'.$el['t_to']?></p>
					</div>
					<div class="discr">
						<p><?=$el['cont']?></p>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php foreach ($list3 as $el) { ?>
				<div class="ell">
					<div class="date">
						<p><?=$el['t_from'].'<br>'.$el['t_to']?></p>
					</div>
					<div class="discr">
						<p><?=$el['cont']?></p>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<p class="pt">* - руководители программы оставляют за собой право вносить изменения в расписание в случае необходимости</p>
	</div>
</section>
<?php } ?>
