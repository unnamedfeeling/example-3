<?php
global $pmeta, $prefix, $options;
// print_r($options);
$price=(!empty($pmeta[$prefix.'files_price'][0])) ? $pmeta[$prefix.'files_price'][0] : null;
$price_glob=(!empty($options['docs']['main-price'])) ? $options['docs']['main-price'] : '#';
$price=(empty($price)) ? $price_glob : $price;
$table=(!empty($pmeta[$prefix.'files_table'][0])) ? $pmeta[$prefix.'files_table'][0] : null;
$table_glob=(!empty($options['docs']['main-table'])) ? $options['docs']['main-table'] : '#';
$table=(empty($table)) ? $table_glob : $table;
$info=(!empty($pmeta[$prefix.'files_info'][0])) ? $pmeta[$prefix.'files_info'][0] : null;
$info_glob=(!empty($options['docs']['main-info'])) ? $options['docs']['main-info'] : '#';
$info=(empty($info)) ? $info_glob : $info;
$rules=(!empty($pmeta[$prefix.'files_rules'][0])) ? $pmeta[$prefix.'files_rules'][0] : null;
$rules_glob=(!empty($options['docs']['main-rules'])) ? $options['docs']['main-rules'] : '#';
$rules=(empty($rules)) ? $rules_glob : $rules;
$wares=(!empty($pmeta[$prefix.'files_wares'][0])) ? $pmeta[$prefix.'files_wares'][0] : null;
$wares_glob=(!empty($options['docs']['main-wares'])) ? $options['docs']['main-wares'] : '#';
$wares=(empty($wares)) ? $wares_glob : $wares;


$form=(!empty($options['socl']['subscr-form'])) ? $options['socl']['subscr-form'] : '[contact-form-7 title="Форма подписки"  html_class="form form-feedback"]';
?>
<section id="block10">
	<div class="container cpad">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<h1>Документы для скачивания</h1>
			</div>
			<div class="col-xs-12 col-sm-3 text-right">
				<a href ="<?=$price?>" download class="btn btn-red openmodal">Прайс-лист</a>
			</div>
			<div class="col-xs-12">
				<nav class="docs">
					<a href="<?=$table?>" download=>Анкета</a> &middot;
					<a href="<?=$info?>" download>Информация для родителей</a> &middot;
					<a href="<?=$rules?>" download>Правила лагеря</a> &middot;
					<?php
					$d='';
					for ($i=1; $i <= 10; $i++) {
						$d.=(!empty($options['docs']['main-docs-ttl'.$i])&&!empty($options['docs']['main-docs'.$i])) ? '<a href="'.$options['docs']['main-docs'.$i].'" download>'.$options['docs']['main-docs-ttl'.$i].'</a> &middot;' : null;
					}
					echo $d;
					?>
					<a href="<?=$wares?>" download class="openmodal">Что взять с собой</a>
				</nav>
			</div>
			<div class="col-xs-12">
				<?= do_shortcode($form);	?>
				<?php
				// <!-- <form action="pos" class="form form-feedback">
				// 	<p>Подпишитесь на новости лагеря и будьте в курсе наших событий</p>
				// 	<div class="text-cont">
				// 		<input type="text" class="input" name="mail" placeholder="E-mail" data-imp="true">
				// 	</div>
				// 	<div class="btn-cont">
				// 		<div class="btn btn-purple">
				// 			<input type="submit" class="btn " value="Подписаться">
				// 		</div>
				// 	</div>
				// </form> -->
				?>
			</div>
		</div>
	</div>
</section>
