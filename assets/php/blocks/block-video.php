<?php
global $pmeta, $prefix, $options;
if(!empty($pmeta[$prefix.'videos_mp4'][0])){
?>
<section id="block5">
	<div class="container cpad">
		<h1>Обычный день в лагере</h1>
		<div class="video-cont">
			<?php if(empty($pmeta[$prefix.'yt_id'][0])){ ?>
			<img class="play b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=$options['tpldir']?>/assets/img/play.png">
			<video class="b-lazy" width="" height="" controls="false" poster="<?=$pmeta[$prefix.'video_poster'][0]?>">
				<source data-src="<?=$pmeta[$prefix.'videos_mp4'][0]?>" type="video/mp4"/>
				<?php if(!empty($pmeta[$prefix.'videos_webm'][0])){ ?><source data-src="<?=$pmeta[$prefix.'videos_webm'][0]?>" type="video/webm"/><?php } ?>
				Плохой браузер. Без шуток. Технологии просмотра html video уже больше 5 лет, а этот браузер ее не поддерживает. Этого динозавра нужно удалить и поставить <a href="https://www.google.com.ua/chrome/browser/desktop/index.html">браузер</a>. Это ссылка на страницу скачивания chrome. Без спама, капчи или смс.
			</video>
			<?php } else { ?>
				<div class="video-container">
					<div class="youtube-player" data-id="<?=$pmeta[$prefix.'yt_id'][0]?>" <?=(!empty($pmeta[$prefix.'video_poster'][0]))?'data-poster="'.$pmeta[$prefix.'video_poster'][0].'"':null?>></div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php } ?>
