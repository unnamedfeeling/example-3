<?php
global $pmeta, $prefix, $options;
$photos=maybe_unserialize($pmeta[$prefix.'photos'][0]);
if(!empty($photos)){
?>
<section id="block11">
	<div class="slick">
		<?php
		foreach ($photos as $key => $val) {
			// echo wp_get_attachment_image_url($key, 'shift-photo-gal');
			printf('<div class="slide"><img src="%s/assets/img/loader.gif" data-lazy="%s" alt="slide1"></div>',
				$options["tpldir"],
				wp_get_attachment_image_url($key, 'shift-photo-gal')
			);
		}
		?>
	</div>
	<div class="containe hidden">
		<div class="col-xs-12 col-sm-9">
			<h1>Не знаешь, какую смену выбрать?</h1>
			<p>Пройди бесплатный тест на сайте и получи ответ.</p>
		</div>
		<div class="col-xs-12 col-sm-3 text-right">
			<button class="btn btn-red" style="display: none">Пройти тест</button>
		</div>
	</div>
</section>
<?php } ?>
