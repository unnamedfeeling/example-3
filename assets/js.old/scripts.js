// $(window).load(function(){
// 	var blazy=new Blazy();
// });

/* viewport width */
function viewport(){
	var e = window,
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}
$.prototype.gotoHref = function(event) {
	var el = $(this.attr('href')),
	t =1500;
	if($(el)[0] === undefined) return false;
	event.preventDefault();
	$('body,html').animate({
		scrollTop: ($(el).offset().top-$('.head').innerHeight())}, t);
};
$.prototype.mvideo = function(){
	$(this).each(function(index, el) {
		var video = $(el)[0];

		if(video === undefined) return false ;
		$(video.parentElement).click(function(event) {
			if (video.paused) {
				video.play();
			} else {
				video.pause();
			}
		});
		$('.pbut').click(function(event) {
			$('.tmenu').toggleClass('active');
		});
		video.addEventListener("play", vplay);
		video.addEventListener("pause", vpause);
	});
};
$.prototype.getp = function(){
	//Размер ползунка
	var div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';

	div.style.visibility = 'hidden';

	document.body.appendChild(div);
	var scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);

	return scrollWidth;
};

var vplay = function(){
	$('video').parent().find('img').addClass('hidden');
};
var vpause = function(){
	$('video').parent().find('img').removeClass('hidden');
};
function gmap (address) {
	var addr=(typeof address!=='undefined' && typeof address==='object') ? address : {lat:46.5879991,lng:30.947694};
	// console.log(addr);
	// console.log(typeof addr);
	if(typeof google === 'undefined'){
		var els = document.getElementsByTagName("script")[0],
			scp = document.createElement("script"),
			func = function () { els.parentNode.insertBefore(scp, els); };
		scp.type = "text/javascript";
		scp.id = 'gmap';
		scp.async = true;
		scp.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyCw72CNd32_vBMZFWy7p-yDlleqdAdHTDY";
		if (window.opera == "[object Opera]") {
			document.addEventListener("DOMContentLoaded", f, false);
		} else { func(); }
		setTimeout(function(){
			var myLatLng=addr,
			map=new google.maps.Map(document.getElementById('map'), {center: myLatLng,zoom: 11,scrollwheel: false}),
			marker=new google.maps.Marker({position: myLatLng,map: map,title: 'Highline'});
		}, 2000)
	} else {
		var myLatLng=addr,
		map=new google.maps.Map(document.getElementById('map'), {center: myLatLng,zoom: 11,scrollwheel: false}),
		marker=new google.maps.Marker({position: myLatLng,map: map,title: 'Highline'});
	}
}
/* viewport width */
$(function(){
	/* placeholder*/
	$('.dump_btn').on('mouseover', function(event) {
		$(this).parent().css({
			zIndex: '2',
		});
	});
	$('.dump_btn').on('mouseout', function(event) {
		$(this).parent().css({
			zIndex: '0',
		});
	});
	$('.dump_btn').click(function(event) {
		$('body,html').animate({
			scrollTop: (0)}, 1500);
	});


	/* placeholder*/
	var auto=false,ap=3000;
	$('#block11 .slick').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		// adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 3,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				dots: false
			}
		}]
	});
	$('.rev-loop').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
	});
	$('.slider').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		swipeToSlide:true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,

			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}]
	});
	$('header nav a, footer nav a').click(function(event) {
		$(this).gotoHref(event);
	});
	$('#block2 .ell').each(function(index, el) {
		$(this).mouseenter(function(event) {

			$(this).css({
				"z-index": '3',
			});
		});
		$(this).mouseleave(function(event) {
			/* Act on the event */
			var r = $(this);
				r.css({
					"z-index": '2',
				});
			setTimeout(function(){
				r.css({
					"z-index": '1',
				});
			}, 500);
		});
	});
	$('header nav,#block10 .icon-menu').click(function(event) {
		$(this).toggleClass('active');
	});
	$('.openmodal').click(function(event) {
		/* Act on the event */
		$(this.hash).toggleClass('active');
	});
	$('.popup-btn').click(function(event) {
		event.preventDefault();
		$('.popup').toggleClass('active');
	});
	// $('.pop-cancle').click(function(event) {
	$(document).on("click", ".pop-cancle", function(event){
		$('.popup ').removeClass('active');
	});

	$('video').mvideo();
	var modclose=function(){
		$('.js-media-modal,#medpop .medpop_btn-close').click(function(event) {
			/* Act on the event */
			$('#medpop').toggleClass('hid');
			//убираем ползунок у body;
			$('body').toggleClass('overh');
			//убираем прыжки страници
			if($('body').hasClass('overh') ){
				$('body')[0].style.paddingRight = $('body').getp()+'px';
			}
			else{
				$('body')[0].style.paddingRight = '0px';
			}
		});
	};
	modclose();
	$('.js-event-modal').click(function(event) {
		id={id : $(this).data('eid')}
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'modal_cont',
				params: id
			},
			success: function (response) {
				// console.log(response);
				$('#medpop').html($.parseHTML(response));
				/* Act on the event */
				$('#medpop').toggleClass('hid');
				//убираем ползунок у body;
				$('body').toggleClass('overh');
				//убираем прыжки страници
				if($('body').hasClass('overh') ){
					$('body')[0].style.paddingRight = $('body').getp()+'px';
				} else {
					$('body')[0].style.paddingRight = '0px';
				}
				modclose();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				// alert(xhr.responseText);
				console.log(xhr.responseText);
			}
		});
	});
	$(document).on('click', '.js-get-more-evt', function(event){
		// var page = $('#events').data("page");
		var page = parseInt(events.dataset.page),
			data={p: page},
			postcount=parseInt($('#events').find('.events_ell').length)+1;
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'get_events',
				params: data
			},
			beforeSend: function(){
				$(".js-get-more-evt").attr('disabled', 'disabled').css('opacity', '0.7');
			},
			success: function (data) {
				// console.log(data);
				$('#events').css('opacity', '0.7');
				// var content=$.parseHTML(data.content.replace('/data-src/g', 'src'));
				var content=$.parseHTML(data.content);
				// console.log($(content).find('img'));
				// $(content).find('img').each(function(){
				// 	// console.log($(this).src);
				// 	var src=$(this).attr('src'),
				// 		img=new Image();
				// 	img.src=src;
				// 	// console.log(src);
				// });
				// setTimeout(function(){
				// 	$('#events').find('.events_loop').append(content);
				// 	$('#events').css('opacity', '1');
				// }, 1000);
				$('#events').find('.events_loop').append(content);
				if(typeof(ajBlazy) == 'undefined'){
					ajBlazy=new Blazy();
				} else {
					ajBlazy.revalidate();
				}
				$('#events').css('opacity', '1');
				// $('#events').data('page', page + 1);
				events.dataset.page++;
				var totalposts=parseInt(data.total_posts.publish);
				// console.log(postcount);
				// console.log(totalposts);
				if(postcount<totalposts){
					$('.js-get-more-evt').removeAttr('disabled').css('opacity', '1');
				} else {
					$('.js-get-more-evt').hide(300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});

		event.preventDefault();

	});
	$(document).on("click", ".js-medpop_btn-reg", function(event){
		/* <![CDATA[ */
		var _wpcf7 = {"loaderUrl":"\/wp\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
		/* ]]> */
		$('#evt-register .wpcf7 > form').wpcf7InitForm();
		var urL = $('#evt-register .wpcf7 > form').attr('action').split('#');
        $('#evt-register .wpcf7 > form').attr('action', "#" + urL[1]);
		$('#evt-register').addClass('active');
		event.preventDefault();
	})
	$(document).on('click', '.js-get-more-photos', function(event){
		// var page = $('#events').data("page");
		var page = parseInt(photo.dataset.page),
			total = parseInt(photo.dataset.total),
			data={p: page, t: total},
			postcount=parseInt($('#photo').find('.media_ell').length);
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'get_photos',
				params: data
			},
			beforeSend: function(){
				$(".js-get-more-photos").attr('disabled', 'disabled').css('opacity', '0.7');
			},
			success: function (data) {
				// console.log(data);
				$('#photo').css('opacity', '0.7');
				// var content=$.parseHTML(data.content.replace('/data-src/g', 'src'));
				var content=$.parseHTML(data.content);
				// console.log($(content).find('img'));
				// $(content).find('img').each(function(){
				// 	// console.log($(this).src);
				// 	var src=$(this).attr('src'),
				// 		img=new Image();
				// 	img.src=src;
				// 	// console.log(src);
				// });
				// setTimeout(function(){
				// 	$('#photo').append(content);
				// 	$('#photo').css('opacity', '1');
				// }, 1000);
				$('#photo').append(content);
				if(typeof(ajBlazy) == 'undefined'){
					ajBlazy=new Blazy();
				} else {
					ajBlazy.revalidate();
				}
				$('#photo').css('opacity', '1');
				// $('#events').data('page', page + 1);
				photo.dataset.page++;
				var totalposts=parseInt(data.total_posts);
				// console.log(postcount);
				// console.log(totalposts);
				if(postcount<totalposts){
					$('.js-get-more-photos').removeAttr('disabled').css('opacity', '1');
				} else {
					$('.js-get-more-photos').hide(300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
		event.preventDefault();
	});
	$(document).on('click', '.js-media_link', function(event){
		var images=$(this).data('imgs'),
			data={i : images};
		if(images.length){
			$.post({
				url: ajax_func.ajax_url,
				dataType: 'json',
				data: {
					action: 'get_media_modal',
					params: data
				},
				beforeSend: function(){
					$(".js-get-more-photos").attr('disabled', 'disabled').css('opacity', '0.7');
				},
				success: function(data){
					// console.log(data);
					var cont=$.parseHTML(data.content)
					$('#media').after(cont);
					var firstSrc=$('#media-slick-2 .slide img').first().data('lazy');
					$('#media-slick-2 .slide img').first().attr('src', firstSrc).removeAttr('data-lazy');
					$('.slick-pop').addClass('active');
					setTimeout(function(){
						$('#media-slick-2').slick({
							accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
							adaptiveHeight: false,
							autoplay:false,
							autoplaySpeed:ap,
							arrows: true,
							// appendDots:$('#media-slick-cont'),
							dots:false,
							nextArrow:'<button type="button" class="slick-next">></button>',
							prevArrow:'<button type="button" class="slick-prev"><</button>',
							slidesToShow: 1,

							// lazyLoad: 'progressive',
							lazyLoad: 'ondemand',
							// customPaging: function(slider, i) {
							//     var img = slider.$slides[i].children[0],
							//     src = img.dataset.mini || img.dataset.lazy || img.src;
							//     return '<img src='+src+' alt="src'+i+'">';
							// },
						});

					}, 100)
					// $(window).resize();
					// $('#media-slick-2').slick('slickGoTo', 1)
					$('.scont_btn-close').click(function(event) {
						/* Act on the event */
						$('.slick-pop').removeClass('active');
						// $('#media-slick-2').unslick();
						$('.slick-pop').remove();
					});
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.responseText);
				}
			});
		}
		event.preventDefault();
	});
	$('.media_h1 label').click(function(event) {
		/* Act on the event */
		$('.media_h1 .active').removeClass('active');
		$(this).addClass('active');
		//= $('#media-slick').slick('getSlick');
		//$('#media-slick,#media-nav-slick').slick('getSlick').resize();
		//var s =$('#media-slick').slick('getSlick');
		//debugger;

		// $('#media-nav-slick').slick('getSlick').slickGoTo();

	});
	$('label[for=s2]').not('.activated').on('click', function(){
		// console.log('test');
		// $('video').mvideo();
		$(this).addClass('activated');
		$('#media-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			var v = $(slick.$slides[nextSlide]).find('video'),
				cv = $(slick.$slides[currentSlide]).find('video'),
				s = $(slick.$slides[nextSlide]).find('source');
			v.attr('poster', v.data('poster'));
			s.attr('src', s.data('lazy'));
			// v.mvideo();
			if(!cv.get(0).paused){cv.get(0).pause();}
			// v.attr('poster', v.data('poster'));
			console.log('paused');
		});
		$('#media-slick').on('init', function(slick, currentSlide, nextSlide){
			// console.log(event);
			// console.log(slick);
			// console.log(currentSlide);
			var v = $(this).find('.slick-current video')
				s = v.find('source');
			v.attr('poster', v.data('poster'));
			s.attr('src', s.data('lazy'));
		});

		$('#media-slick').slick({
			accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
			adaptiveHeight: true,
			autoplay:false,
			autoplaySpeed:ap,
			arrows: false,
			appendDots:$('.media-slick-dots'),
			dots:false,
			lazyLoad: 'progressive',
			touchMove:false,
			swipe:false,
		});
		$('#media-nav-slick').slick({
			asNavFor:$('#media-slick'),
			slidesToShow: 5,
			slidesToScroll: 1,
			swipeToSlide:true,
			focusOnSelect: true,
			centerMode: true,
			arrows:false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			}]
		});
	})

	$('#media-slick-2').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		autoplaySpeed:ap,
		arrows: true,
		appendDots:$('#media-slick-cont'),
		dots:true,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',

		lazyLoad: 'progressive',
		customPaging: function(slider, i) {
		    var img = slider.$slides[i].children[0],
		    src = img.dataset.mini || img.dataset.lazy || img.src;
		    return '<img src='+src+' alt="src'+i+'">';
		},
	});
	$('#media-slick-cont li').click(function(event) {
		/* Act on the event */
		$('.slick-pop').addClass('active');
	});
	$('.scont_btn-close').click(function(event) {
		/* Act on the event */
		$('.slick-pop').removeClass('active');
	});
});

var maploc,scrflag=true;
var handler = function(){
	$('.shift').css({'min-height':'100%'});
	$('.shift').css({'min-height':$('.container.abs').height()+30+'px'});
	maploc = $('#map')[0] !== undefined ? $('#map').offset().top : false;
	var height_footer = $('footer').height();
	var height_header = $('header').height();
	$('.content').css({'padding-bottom':height_footer, 'padding-top':0});
	$('.js-pad-top').css({'padding-top':height_header});
	var viewport_wid = viewport().width;
	if (viewport_wid <= 991) {

	}

};
var mload = function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	}
	$('img').not('.b-lazy').each(function(index, el) {
		this.src = this.dataset.src || this.src ;
	});
	$('body').removeClass('loaded');
	$('video').on("contextmenu", false);
	handler();
	var blazy=new Blazy();
};
var mresize = function(){
	handler();

};
var mscroll = function(){
	var scrlt= $(window).scrollTop() + $(window).height(), map = $('#map'), h=500;

	if($('#gmap').length===0&&$('#map').length){
		$(window).scroll(function() {
			// if((document.body.classList.contains('page-template-template-school')||document.body.classList.contains('page-template-template-main'))&&$(window).scrollTop() > amountScrolled&&$('#gmap').length===0){
			if($(window).scrollTop() > h&&$('#gmap').length===0){
				var els = document.getElementsByTagName("script")[0],
					scp = document.createElement("script"),
					func = function () { els.parentNode.insertBefore(scp, els); };
				scp.type = "text/javascript";
				scp.id = 'gmap';
				scp.async = true;
				scp.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyCw72CNd32_vBMZFWy7p-yDlleqdAdHTDY";
				if (window.opera == "[object Opera]") {
					document.addEventListener("DOMContentLoaded", f, false);
				} else { func(); }
			}
		});
	}
	maploc =($('#map')[0] !== undefined) ? $('#map').offset().top : false;
	if(  maploc && scrlt >= maploc && !map.hasClass('loaded') ){
		map.addClass('loaded');
		// gmap(" Одесса, пгт. Черноморское, ул. Уютная, 1");
		// gmap("пгт. Грибовка");
		// gmap();
		// console.log(typeof additionalData);
		if(typeof additionalData==='undefined'||typeof additionalData.address_camp_map==='undefined'){
			gmap();
		} else {
			// console.log(typeof additionalData);
			gmap(additionalData.address_camp_map);
		}
		// console.log(additionalData.address_camp_map);
		// gmap(additionalData.address_camp_map);
	}

	if($(window).scrollTop()<$(window).height()){
		$('.dump_btn').addClass('hidden');
	}
	else{
		$('.dump_btn').removeClass('hidden');
	}

};
$(window).bind('load', mload);
$(window).bind('resize', mresize);
$(window).bind('scroll', mscroll);

$(document).on('mousewheel DOMMouseScroll', function(event) {
	$('body,html').stop(true, false);
});
