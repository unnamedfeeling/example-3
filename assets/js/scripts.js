// $(window).load(function(){
// 	var blazy=new Blazy();
// });

/* viewport width */
function viewport(){
	var e = window,
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}
// wpcf7 submit to amo callback
// function amoAdopt(serdata){
// 	// console.log(ajax_func.ajax_url);
// 	// console.log(serdata);
// 	$.post({
// 		url: ajax_func.ajax_url,
// 		data: {
// 			action: 'startime_amocrm_integrate',
// 			params: serdata
// 		},
// 		success: function (response) {
// 			// console.log(response);
// 		},
// 		error: function (xhr, ajaxOptions, thrownError) {
// 			alert(xhr.responseText);
// 		}
// 	})
// }
// document.addEventListener( 'wpcf7mailsent', function( event ) {
//     var formid=event.detail.contactFormId;
// 	// console.log(event.detail);
// 	// if(formid=='6'||formid=='108'){
// 		// var serdata=$(this).closest('form').serialize();
// 		var serdata=event.detail.inputs;
// 		// console.log(serdata);
// 		amoAdopt(serdata);
// 	// }
// 	if ( '6' == event.detail.contactFormId  ) {
// 		var fid=event.detail.id;
// 		$('#'+fid).find('form').append('<h5 class="success" style="text-align: center;">Спасибо, мы с Вами свяжемся!</h5>');
// 		$('#'+fid).find('.row, .wpcf7-response-output, h1').addClass('hidden');
// 		setTimeout(function() {
// 			$('.popup').toggleClass('active');
// 			// console.log('test');
// 			setTimeout(function() {
// 				$('#'+fid).find('form').find('h5.success').remove();
// 				$('#'+fid).find('.row, .wpcf7-response-output, h1').removeClass('hidden');
// 				$('#'+fid).find('.wpcf7-response-output').addClass('wpcf7-display-none').removeClass('wpcf7-mail-sent-ok').removeAttr('style').empty();
// 				// $('#'+fid).find('.wpcf7-response-output').removeClass('wpcf7-mail-sent-ok');
// 			}, 1000);
// 		}, 2000);
//
// 	}
// }, false );

// working with get params
function getSearchParameters() {
	var prmstr = window.location.search.substr(1);
	return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
	var params = {};
	var prmarr = prmstr.split("&");
	for ( var i = 0; i < prmarr.length; i++) {
		var tmparr = prmarr[i].split("=");
		params[tmparr[0]] = tmparr[1];
	}
	return params;
}


$.prototype.gotoHref = function(event) {
	var el = $(this.attr('href')),
	t =1500;
	if($(el)[0] === undefined) return false;
	event.preventDefault();
	$('body,html').animate({
		scrollTop: ($(el).offset().top-$('.head').innerHeight())}, t);
};
$.prototype.mvideo = function(){
	$(this).each(function(index, el) {
		var video = $(el)[0];

		if(video === undefined) return false ;
		$(video.parentElement).click(function(event) {
			if (video.paused) {
				video.play();
			} else {
				video.pause();
			}
		});
		$('.pbut').click(function(event) {
			$('.tmenu').toggleClass('active');
		});
		video.addEventListener("play", vplay);
		video.addEventListener("pause", vpause);
	});
};
$.prototype.getp = function(){
	//Размер ползунка
	var div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';

	div.style.visibility = 'hidden';

	document.body.appendChild(div);
	var scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);

	return scrollWidth;
};
$.prototype.jschheight = function(argument){
	$(this).each(function(index, el) {
		$(this).children().height('auto');
		$(this).children().height($(this).height());
	});
};

var vplay = function(){
	$('video').parent().find('img').addClass('hidden');
};
var vpause = function(){
	$('video').parent().find('img').removeClass('hidden');
};
function gmap (address) {
	var addr=(typeof address!=='undefined' && typeof address==='object') ? address : {lat:46.5879991,lng:30.947694};
	// console.log(addr);
	// console.log(typeof addr);
	if(typeof google === 'undefined'){
		var els = document.getElementsByTagName("script")[0],
			scp = document.createElement("script"),
			func = function () { els.parentNode.insertBefore(scp, els); };
		scp.type = "text/javascript";
		scp.id = 'gmap';
		scp.async = true;
		scp.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyCw72CNd32_vBMZFWy7p-yDlleqdAdHTDY";
		if (window.opera == "[object Opera]") {
			document.addEventListener("DOMContentLoaded", f, false);
		} else { func(); }
		setTimeout(function(){
			var myLatLng=addr,
			map=new google.maps.Map(document.getElementById('map'), {center: myLatLng,zoom: 11,scrollwheel: false}),
			marker=new google.maps.Marker({position: myLatLng,map: map,title: 'Highline'});
		}, 2000)
	} else {
		var myLatLng=addr,
		map=new google.maps.Map(document.getElementById('map'), {center: myLatLng,zoom: 11,scrollwheel: false}),
		marker=new google.maps.Marker({position: myLatLng,map: map,title: 'Highline'});
	}
}
/* viewport width */
$(function(){
	/* placeholder*/
	$('.dump_btn').on('mouseover', function(event) {
		$(this).parent().css({
			zIndex: '2',
		});
	});
	$('.dump_btn').on('mouseout', function(event) {
		$(this).parent().css({
			zIndex: '0',
		});
	});
	$('.dump_btn').click(function(event) {
		$('body,html').animate({
			scrollTop: (0)}, 1500);
	});


	/* placeholder*/
	var auto=false,ap=3000;
	$('#block11 .slick').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		// adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 3,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				dots: false
			}
		}]
	});
	$('.rev-loop').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
	});
	$('.slider').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		swipeToSlide:true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,

			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}]
	});
	$('header nav a, footer nav a').click(function(event) {
		$(this).gotoHref(event);
	});
	$('#block2 .ell').each(function(index, el) {
		$(this).mouseenter(function(event) {

			$(this).css({
				"z-index": '3',
			});
		});
		$(this).mouseleave(function(event) {
			/* Act on the event */
			var r = $(this);
				r.css({
					"z-index": '2',
				});
			setTimeout(function(){
				r.css({
					"z-index": '1',
				});
			}, 500);
		});
	});
	$('header nav,#block10 .icon-menu').click(function(event) {
		$(this).toggleClass('active');
	});
	$('.openmodal').click(function(event) {
		/* Act on the event */
		$(this.hash).toggleClass('active');
	});
	$('.popup-btn').click(function(event) {
		event.preventDefault();
		$('.popup').toggleClass('active');
	});
	// $('.pop-cancle').click(function(event) {
	$(document).on("click", ".pop-cancle", function(event){
		$('.popup ').removeClass('active');
	});

	$('video').mvideo();
	var modclose=function(){
		$('.js-media-modal,#medpop .medpop_btn-close').click(function(event) {
			/* Act on the event */
			$('#medpop').toggleClass('hid');
			//убираем ползунок у body;
			$('body').toggleClass('overh');
			//убираем прыжки страници
			if($('body').hasClass('overh') ){
				$('body')[0].style.paddingRight = $('body').getp()+'px';
			}
			else{
				$('body')[0].style.paddingRight = '0px';
			}
		});
	};
	modclose();
	$('.js-event-modal').click(function(event) {
		id={id : $(this).data('eid')}
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'modal_cont',
				params: id
			},
			success: function (response) {
				// console.log(response);
				$('#medpop').html($.parseHTML(response));
				/* Act on the event */
				$('#medpop').toggleClass('hid');
				//убираем ползунок у body;
				$('body').toggleClass('overh');
				//убираем прыжки страници
				if($('body').hasClass('overh') ){
					$('body')[0].style.paddingRight = $('body').getp()+'px';
				} else {
					$('body')[0].style.paddingRight = '0px';
				}
				modclose();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				// alert(xhr.responseText);
				console.log(xhr.responseText);
			}
		});
	});
	$(document).on('click', '.js-get-more-evt', function(event){
		// var page = $('#events').data("page");
		var page = parseInt(events.dataset.page),
			data={p: page},
			postcount=parseInt($('#events').find('.events_ell').length)+1;
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'get_events',
				params: data
			},
			beforeSend: function(){
				$(".js-get-more-evt").attr('disabled', 'disabled').css('opacity', '0.7');
			},
			success: function (data) {
				// console.log(data);
				$('#events').css('opacity', '0.7');
				// var content=$.parseHTML(data.content.replace('/data-src/g', 'src'));
				var content=$.parseHTML(data.content);
				// console.log($(content).find('img'));
				// $(content).find('img').each(function(){
				// 	// console.log($(this).src);
				// 	var src=$(this).attr('src'),
				// 		img=new Image();
				// 	img.src=src;
				// 	// console.log(src);
				// });
				// setTimeout(function(){
				// 	$('#events').find('.events_loop').append(content);
				// 	$('#events').css('opacity', '1');
				// }, 1000);
				$('#events').find('.events_loop').append(content);
				if(typeof(ajBlazy) == 'undefined'){
					ajBlazy=new Blazy();
				} else {
					ajBlazy.revalidate();
				}
				$('#events').css('opacity', '1');
				// $('#events').data('page', page + 1);
				events.dataset.page++;
				var totalposts=parseInt(data.total_posts.publish);
				// console.log(postcount);
				// console.log(totalposts);
				if(postcount<totalposts){
					$('.js-get-more-evt').removeAttr('disabled').css('opacity', '1');
				} else {
					$('.js-get-more-evt').hide(300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});

		event.preventDefault();

	});
	$(document).on("click", ".js-medpop_btn-reg", function(event){
		/* <![CDATA[ */
		var _wpcf7 = {"loaderUrl":"\/wp\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
		/* ]]> */
		$('#evt-register .wpcf7 > form').wpcf7InitForm();
		var urL = $('#evt-register .wpcf7 > form').attr('action').split('#');
        $('#evt-register .wpcf7 > form').attr('action', "#" + urL[1]);
		$('#evt-register').addClass('active');
		event.preventDefault();
	})
	$(document).on('click', '.js-get-more-photos', function(event){
		// var page = $('#events').data("page");
		var page = parseInt(photo.dataset.page),
			total = parseInt(photo.dataset.total),
			data={p: page, t: total},
			postcount=parseInt($('#photo').find('.media_ell').length);
		$.post({
			url: ajax_func.ajax_url,
			dataType: 'json',
			data: {
				action: 'get_photos',
				params: data
			},
			beforeSend: function(){
				$(".js-get-more-photos").attr('disabled', 'disabled').css('opacity', '0.7');
			},
			success: function (data) {
				// console.log(data);
				$('#photo').css('opacity', '0.7');
				// var content=$.parseHTML(data.content.replace('/data-src/g', 'src'));
				var content=$.parseHTML(data.content);
				// console.log($(content).find('img'));
				// $(content).find('img').each(function(){
				// 	// console.log($(this).src);
				// 	var src=$(this).attr('src'),
				// 		img=new Image();
				// 	img.src=src;
				// 	// console.log(src);
				// });
				// setTimeout(function(){
				// 	$('#photo').append(content);
				// 	$('#photo').css('opacity', '1');
				// }, 1000);
				$('#photo').append(content);
				if(typeof(ajBlazy) == 'undefined'){
					ajBlazy=new Blazy();
				} else {
					ajBlazy.revalidate();
				}
				$('#photo').css('opacity', '1');
				// $('#events').data('page', page + 1);
				photo.dataset.page++;
				var totalposts=parseInt(data.total_posts);
				// console.log(postcount);
				// console.log(totalposts);
				if(postcount<totalposts){
					$('.js-get-more-photos').removeAttr('disabled').css('opacity', '1');
				} else {
					$('.js-get-more-photos').hide(300);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.responseText);
			}
		});
		event.preventDefault();
	});
	$(document).on('click', '.js-media_link', function(event){
		var images=$(this).data('imgs'),
			pos=$(this).data('position'),
			data={i : images, p : pos};
		if(images.length){
			$.post({
				url: ajax_func.ajax_url,
				dataType: 'json',
				data: {
					action: 'get_media_modal',
					params: data
				},
				beforeSend: function(){
					$(".js-get-more-photos").attr('disabled', 'disabled').css('opacity', '0.7');
				},
				success: function(data){
					// console.log(data);
					var cont=$.parseHTML(data.content)
					$('#media').after(cont);
					var firstSrc=$('#media-slick-2 .slide img').first().data('lazy');
					$('#media-slick-2 .slide img').first().attr('src', firstSrc).removeAttr('data-lazy');
					$('.slick-pop').addClass('active');
					setTimeout(function(){
						$('#media-slick-2').slick({
							accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
							// adaptiveHeight: true,
							adaptiveHeight: false,
							autoplay:false,
							fade: true,
							autoplaySpeed:ap,
							arrows: true,
							// appendDots:$('#media-slick-cont'),
							dots:false,
							nextArrow:'<button type="button" class="slick-next">></button>',
							prevArrow:'<button type="button" class="slick-prev"><</button>',
							slidesToShow: 1,
							// lazyLoad: 'progressive',
							// lazyLoad: 'ondemand',
							lazyLoad: 'anticipated',
							// customPaging: function(slider, i) {
							//     var img = slider.$slides[i].children[0],
							//     src = img.dataset.mini || img.dataset.lazy || img.src;
							//     return '<img src='+src+' alt="src'+i+'">';
							// },
						});
						$('#media-slick-2').slick('slickGoTo', pos);
						$('#media-slick-2').slick('slickGoTo', 0);
						$('#media-slick-2').slick('slickGoTo', pos);

					}, 100)
					// $(window).resize();
					// $('#media-slick-2').slick('slickGoTo', 1)
					$('.scont_btn-close').click(function(event) {
						/* Act on the event */
						$('.slick-pop').removeClass('active');
						// $('#media-slick-2').unslick();
						$('.slick-pop').remove();
					});
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.responseText);
				}
			});
		}
		event.preventDefault();
	});
	$('.media_h1 label').click(function(event) {
		/* Act on the event */
		$('.media_h1 .active').removeClass('active');
		$(this).addClass('active');
		//= $('#media-slick').slick('getSlick');
		//$('#media-slick,#media-nav-slick').slick('getSlick').resize();
		//var s =$('#media-slick').slick('getSlick');
		//debugger;

		// $('#media-nav-slick').slick('getSlick').slickGoTo();

	});
	$('label[for=s2]').not('.activated').on('click', function(){
		// console.log('test');
		// $('video').mvideo();
		$(this).addClass('activated');
		$('#media-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			var v = $(slick.$slides[nextSlide]).find('video'),
				cv = $(slick.$slides[currentSlide]).find('video'),
				s = $(slick.$slides[nextSlide]).find('source');
			v.attr('poster', v.data('poster'));
			s.attr('src', s.data('lazy'));
			// v.mvideo();
			if(cv.length&&!cv.get(0).paused){cv.get(0).pause();}
			// v.attr('poster', v.data('poster'));
			// console.log('paused');
		});
		$('#media-slick').on('init', function(slick, currentSlide, nextSlide){
			// console.log(event);
			// console.log(slick);
			// console.log(currentSlide);
			var v = $(this).find('.slick-current video')
				s = v.find('source');
			v.attr('poster', v.data('poster'));
			s.attr('src', s.data('lazy'));
		});

		$('#media-slick').slick({
			accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
			adaptiveHeight: true,
			autoplay:false,
			autoplaySpeed:ap,
			arrows: false,
			appendDots:$('.media-slick-dots'),
			dots:false,
			lazyLoad: 'progressive',
			touchMove:false,
			swipe:false,
		});
		$('#media-nav-slick').slick({
			asNavFor:$('#media-slick'),
			slidesToShow: 5,
			slidesToScroll: 1,
			swipeToSlide:true,
			focusOnSelect: true,
			centerMode: true,
			arrows:false,
			responsive: [{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			}]
		});
	})

	$('#media-slick-2').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		autoplaySpeed:ap,
		arrows: true,
		appendDots:$('#media-slick-cont'),
		dots:true,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',

		// lazyLoad: 'progressive',
		lazyLoad: 'ondemand',
		customPaging: function(slider, i) {
		    var img = slider.$slides[i].children[0],
		    src = img.dataset.mini || img.dataset.lazy || img.src;
		    return '<img src='+src+' alt="src'+i+'">';
		},
	});
	$('#media-slick-cont li').click(function(event) {
		/* Act on the event */
		$('.slick-pop').addClass('active');
	});
	$('.scont_btn-close').click(function(event) {
		/* Act on the event */
		$('.slick-pop').removeClass('active');
	});

	var urlParams = getSearchParameters(),
		findGet=window.location.href.indexOf('?'),
		formHidInputs='',
		formHidInputs_sec='',
		jsonParams={};
	urlParams.utm_useragent=navigator.userAgent;
	if (findGet!=-1) {
		urlParams.utm_page=window.location.href.substring(0, findGet);
	} else {
		urlParams.utm_page=window.location.href;
	}
	urlParams.utm_prevpage=document.referrer;

	$.getJSON('//freegeoip.net/json/?callback=?', function(data) {
		// console.log(JSON.stringify(data));
		jsonParams.utm_ip=data.ip;
		jsonParams.utm_city=data.city;
		jsonParams.utm_country=data.country_name;
	}).always(function(){
		// console.log(urlParams);
		// console.log(jsonParams);
		if ($('.wpcf7-form').length) {
			// console.log(urlParams);
			Object.keys(urlParams).forEach(function(key,index) {
				formHidInputs+='<input type="hidden" name="'+key+'" value="'+urlParams[key]+'">';
			});
			if (typeof jsonParams!=='undefined') {
				Object.keys(jsonParams).forEach(function(key,index) {
					formHidInputs_sec+='<input type="hidden" name="'+key+'" value="'+jsonParams[key]+'">';
				});
			}
			// console.log(formHidInputs);
			$('.wpcf7-form').each(function(index){
				// var thForm = $(this);
				if (typeof formHidInputs!=='undefined'&&formHidInputs!=='') {
					$(this).prepend(formHidInputs);
				}
				if (typeof formHidInputs_sec!=='undefined'&&formHidInputs_sec!=='') {
					$(this).prepend(formHidInputs_sec);
				}
			});
		}
	});
});

var maploc,scrflag=true;
var handler = function(){
	$('.shift').css({'min-height':'100%'});
	$('.shift').css({'min-height':$('.container.abs').height()+30+'px'});
	maploc = $('#map')[0] !== undefined ? $('#map').offset().top : false;
	// var height_footer = $('footer').height();
	// var height_header = $('header').height();
	var height_footer=foot.offsetHeight,
		height_header=head.offsetHeight;
	$('.content').css({'padding-bottom':height_footer, 'padding-top':0});
	$('.js-pad-top').css({'padding-top':height_header});
	var viewport_wid = viewport().width;
	if (viewport_wid > 991) {
		$('.js-child-height').jschheight();
	}
	else{
		$('.js-child-height').children().height('auto')
	}
};
var mload = function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	}
	$('img').not('.b-lazy').each(function(index, el) {
		this.src = this.dataset.src || this.src ;
	});
	$('body').removeClass('loaded');
	$('video').on("contextmenu", false);
	handler();
	var blazy=new Blazy();
};
var mresize = function(){
	handler();

};
var mscroll = function(){
	var scrlt= $(window).scrollTop() + $(window).height(), map = $('#map'), h=500;

	if($('#gmap').length===0&&$('#map').length){
		$(window).scroll(function() {
			// if((document.body.classList.contains('page-template-template-school')||document.body.classList.contains('page-template-template-main'))&&$(window).scrollTop() > amountScrolled&&$('#gmap').length===0){
			if($(window).scrollTop() > h&&$('#gmap').length===0){
				var els = document.getElementsByTagName("script")[0],
					scp = document.createElement("script"),
					func = function () { els.parentNode.insertBefore(scp, els); };
				scp.type = "text/javascript";
				scp.id = 'gmap';
				scp.async = true;
				scp.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyCw72CNd32_vBMZFWy7p-yDlleqdAdHTDY";
				if (window.opera == "[object Opera]") {
					document.addEventListener("DOMContentLoaded", f, false);
				} else { func(); }
			}
		});
	}
	maploc =($('#map')[0] !== undefined) ? $('#map').offset().top : false;
	if(  maploc && scrlt >= maploc && !map.hasClass('loaded') ){
		map.addClass('loaded');
		// gmap(" Одесса, пгт. Черноморское, ул. Уютная, 1");
		// gmap("пгт. Грибовка");
		// gmap();
		// console.log(typeof additionalData);
		if(typeof additionalData==='undefined'||typeof additionalData.address_camp_map==='undefined'){
			gmap();
		} else {
			// console.log(typeof additionalData);
			gmap(additionalData.address_camp_map);
		}
		// console.log(additionalData.address_camp_map);
		// gmap(additionalData.address_camp_map);
	}

	if($(window).scrollTop()<100){
		$('.dump_btn').addClass('hidden');
	}
	else{
		$('.dump_btn').removeClass('hidden');
	}

};
$(window).bind('load', mload);
$(window).bind('resize', mresize);
$(window).bind('scroll', mscroll);

$(document).on('mousewheel DOMMouseScroll', function(event) {
	$('body,html').stop(true, false);
});

/*Youtube videos*/
var player;
function ytimg(){
	var div, n,
		v = document.getElementsByClassName("youtube-player");
	for (n = 0; n < v.length; n++) {
		div = document.createElement("div");
		div.id=v[n].dataset.id;
		div.setAttribute("data-id", v[n].dataset.id);
		div.innerHTML = labnolThumb(v[n].dataset.id);
		div.onclick = labnolIframe;
		v[n].appendChild(div);
	}
}
function labnolThumb(id) {
	// var thumb, play = '<div class="play"></div>';
	var thumb, play = '<img class="play" src="'+servdata.tpldir+'/assets/img/play.png">';
	var vholdr=$('[data-id="'+id+'"]');
	thumb=(vholdr.data('poster')!=='')?'<img class="vidimg b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" '+((!vholdr.parent().parent().hasClass('slide'))?'data-src':'data-lazy')+'="'+vholdr.data('poster')+'">' : '<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="https://i.ytimg.com/vi/ID/hqdefault.jpg">';
	// console.log(thumb);
	return thumb.replace("/ID/hqdefault.jpg", "/"+id+"/hqdefault.jpg") + play;
}
function labnolIframe() {
	var w=this.offsetWidth,
		h=this.offsetHeight-6,
		v_id=this.dataset.id,
		params={
			height: h,
			width: w,
			videoId: v_id,
			events: {
				'onReady': onPlayerReady
			}
		};

	if(typeof YT === 'undefined'){
		var els = document.getElementsByTagName("script")[0],
			scp = document.createElement("script"),
			func = function () { els.parentNode.insertBefore(scp, els); };
		scp.type = "text/javascript";
		scp.id = 'youtube';
		scp.async = true;
		scp.src = "//www.youtube.com/iframe_api";
		if (window.opera == "[object Opera]") {
			document.addEventListener("DOMContentLoaded", f, false);
		} else { func(); }
		if (scp.readyState) { // IE, incl. IE9
			scp.onreadystatechange = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 800)
			}
		} else {
			scp.onload = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 800)
			}
		}
	} else {
		setTimeout(function(){
			player = new YT.Player(v_id, params);
		}, 800)
	}
}
function onPlayerReady(event) {
	event.target.playVideo();
}
function stopVideo() {
	player.stopVideo();
}
function pauseVid(event) {
	player.pauseVideo();
}
ytimg();
