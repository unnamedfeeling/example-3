/* viewport width */
function viewport(){
	var e = window, 
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
$.prototype.goto = function(event) {
	var el = $(this.attr('href')),
	t =1500;
	if($(el)[0] === undefined) return false;
	event.preventDefault();
	$('body,html').animate({
		scrollTop: ($(el).offset().top-$('.head').innerHeight())}, t);
};
$.prototype.mvideo = function(){
	$(this).each(function(index, el) {
		var video = $(el)[0];

		if(video == undefined) return false ;
		$(video.parentElement).click(function(event) {
			if (video.paused) {
				video.play();
			} else {
				video.pause();
			}
		});
		$('.pbut').click(function(event) {
			$('.tmenu').toggleClass('active');
		});
		video.addEventListener("play", vplay);
		video.addEventListener("pause", vpause);
	});
}
$.prototype.getp = function(){
	//Размер ползунка
	var div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';

	div.style.visibility = 'hidden';

	document.body.appendChild(div);
	var scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);

	return scrollWidth;
}
$.prototype.jschheight = function(argument){
	$(this).each(function(index, el) {
		$(this).children().height('auto');
		$(this).children().height($(this).height());
	});
};
var vplay = function(){
	$('video').parent().find('img').addClass('hidden');
}
var vpause = function(){
	$('video').parent().find('img').removeClass('hidden');
}
function gmap (address) {
	var myLatLng={lat:46.5879991,lng:30.947694},
	map=new google.maps.Map(document.getElementById('map'), {center: myLatLng,zoom: 11,scrollwheel: false}),
	marker=new google.maps.Marker({position: myLatLng,map: map,title: 'Highline'});
}
/* viewport width */
$(function(){
	/* placeholder*/	
	$('.dump_btn').on('mouseover', function(event) {
		$(this).parent().css({
			zIndex: '2',
		});
	});
	$('.dump_btn').on('mouseout', function(event) {
		$(this).parent().css({
			zIndex: '0',
		});
	});
	$('.dump_btn').click(function(event) {
		$('body,html').animate({
			scrollTop: (0)}, 1500);
	});

	
	/* placeholder*/	   
	var auto=true,ap=3000;
	$('#block11 .slick').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 3,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				dots: false
			}
		}]
	});
	$('.rev-loop').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		arrows: true,
		centerMode: false,
		dots:false,
		focusOnSelect: true,	//Enable focus on selected element (click)
		swipeToSlide:true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
	});
	$('.slider').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:auto,
		autoplaySpeed:ap,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		swipeToSlide:true,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				 
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}]
	});
	$('header nav a, footer nav a').click(function(event) {
		$(this).goto(event);
	});
	$('#block2 .ell').each(function(index, el) {
		$(this).mouseenter(function(event) {
			
			$(this).css({
				"z-index": '3',
			});
		});
		$(this).mouseleave(function(event) {
			/* Act on the event */
			var r = $(this);
				r.css({
					"z-index": '2',
				});
			setTimeout(function(){
				r.css({
					"z-index": '1',
				});
			}, 500)
		});
	});
	$('header nav,#block10 .icon-menu').click(function(event) {
		$(this).toggleClass('active');
	});
	$('.openmodal').click(function(event) {
		/* Act on the event */
		$(this.hash).toggleClass('active');
	});
	$('.popup-btn').click(function(event) {
		event.preventDefault();
		$('.popup').toggleClass('active');
	});
	$('.pop-cancle').click(function(event) {
		$('.popup ').removeClass('active');
	});
	
	$('video').mvideo();
	$('.js-media-modal,.js-event-modal,#medpop .medpop_btn-close').click(function(event) {
		/* Act on the event */
		$('#medpop').toggleClass('hid');
		//убираем ползунок у body;
		$('body').toggleClass('overh');
		//убираем прыжки страници
		if($('body').hasClass('overh') ){
			$('body')[0].style.paddingRight = $('body').getp()+'px';
		}
		else{
			$('body')[0].style.paddingRight = '0px';
		}
	});
	$('.media_h1 label').click(function(event) {
		/* Act on the event */
		$('.media_h1 .active').removeClass('active');
		$(this).addClass('active');
		//= $('#media-slick').slick('getSlick');
		//$('#media-slick,#media-nav-slick').slick('getSlick').resize();
		//var s =$('#media-slick').slick('getSlick');
		//debugger;
		$('#media-nav-slick').slick('getSlick').slickGoTo();

	});
	$('#media-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var v = $(slick.$slides[currentSlide]).find('video')[0];
		if(v){v.pause();}
		console.log('paused');
	});

	$('#media-slick').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		autoplaySpeed:ap,
		arrows: false, 
		appendDots:$('.media-slick-dots'),
		dots:false,
		lazyLoad: 'progressive',
		touchMove:false,
		swipe:false,
	});
	$('#media-nav-slick').slick({
		asNavFor:$('#media-slick'),
		slidesToShow: 5,
		slidesToScroll: 1,
		swipeToSlide:true,
		focusOnSelect: true,
		centerMode: true,
		arrows:false,

		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				infinite: true,
				dots: false
			}
		}]
	})

	$('#media-slick-2').slick({
		accessibility:true ,//Включает табулируя и клавиша со стрелкой навигации
		adaptiveHeight: true,
		autoplay:false,
		autoplaySpeed:ap,
		arrows: true, 
		appendDots:$('#media-slick-cont'),
		dots:true,
		nextArrow:'<button type="button" class="slick-next">></button>',
		prevArrow:'<button type="button" class="slick-prev"><</button>',
		
		lazyLoad: 'progressive',
		customPaging: function(slider, i) {
		    var img = slider.$slides[i].children[0],
		    src = img.dataset.mini || img.dataset.lazy || img.src;
		    return '<img src='+src+' alt="src'+i+'">';
		},
	});
	$('#media-slick-cont li').click(function(event) {
		/* Act on the event */
		$('.slick-pop').addClass('active');
	});
	$('.scont_btn-close').click(function(event) {
		/* Act on the event */
		$('.slick-pop').removeClass('active');
	});
});

var maploc,scrflag=true;
var handler = function(){
	$('.shift').css({'min-height':'100%'});
	$('.shift').css({'min-height':$('.container.abs').height()+30+'px'});
	maploc = $('#map')[0] != undefined ? $('#map').offset().top : false;
	var height_footer = $('footer').height();
	var height_header = $('header').height();
	$('.content').css({'padding-bottom':height_footer, 'padding-top':0});
	$('.js-pad-top').css({'padding-top':height_header})
	var viewport_wid = viewport().width;
	if (viewport_wid > 991) {
		$('.js-child-height').jschheight();
	}
	else{
		$('.js-child-height').children().height('auto')
	}
}
var mload = function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	};
	$('img').each(function(index, el) {
		this.src = this.dataset.src || this.src ;
	});
	$('body').removeClass('loaded');
	$('video').on("contextmenu", false);
	handler();
}
var mresize = function(){
	handler();
	
}
var mscroll = function(){

	var scrlt= $(window).scrollTop() + $(window).height(), map = $('#map');
	maploc =$('#map')[0] != undefined ? $('#map').offset().top : false;
	if(  maploc && scrlt >= maploc && !map.hasClass('loaded') ){
		map.addClass('loaded');
		gmap(" Одесса, пгт. Черноморское, ул. Уютная, 1");
	}
	
	if($(window).scrollTop()<$(window).height()){
		$('.dump_btn').addClass('hidden');
	}
	else{
		$('.dump_btn').removeClass('hidden');
	}

}
$(window).bind('load', mload);
$(window).bind('resize', mresize);
$(window).bind('scroll', mscroll);

$(document).on('mousewheel DOMMouseScroll', function(event) {
	$('body,html').stop(true, false);
});



