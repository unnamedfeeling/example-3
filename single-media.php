<?php
$pmeta=get_post_meta( $post->ID, '', false );
$prefix='startimecamp_';
if($pmeta[$prefix.'post_type'][0]=='video'){
	wp_redirect( get_post_type_archive_link( 'media' ), 301 );
} else {
	get_header();
	$images=array();
	foreach (maybe_unserialize( $pmeta[$prefix.'mediaimages'][0] ) as $key => $value) {
		$images[]=$key;
	}
	$phthumb=wp_get_attachment_image_src($pmeta['_thumbnail_id'][0], 'photo-loop-img');
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="media" class="js-pad-top">
				<div id="media" class="container">
					<div class="single-photo-header media_cont">
						<div class="media_ell">
							<?php
							if(!empty($phthumb)){
								// echo remove_width_attribute($phthumb);
								printf('<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="%s" class="b-lazy">',
									$phthumb[0]
								);
							} else {
							?>
							<img src="<?=$options['tpldir']?>/assets/img/preimg.jpg" alt="preimg">
							<?php } ?>
							<p class="media_date"><?php if(!empty($phmeta[$prefix.'gallery_date'][0])) echo strtr(date('d M', $phmeta[$prefix.'gallery_date'][0]), $options['translate']);?></p>
							<p class="media_name"><?=$post->post_title?></p>
							<p class="media_count"><span><?=count($images)?></span> фото</p>
						</div>
					</div>
					<div class="media-cont">
						<?php
						$i=0;
						foreach ($images as $key) {
							printf( '<div class="media_ell"><a href="#" class="media_link js-media_link" target="_blank" data-imgs="%s" data-position="%s"></a><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="%s" class="b-lazy"></div>',
								implode(',', $images),
								$i,
								wp_get_attachment_image_url($key, 'photo-loop-img')
							);
							$i++;
						}
						?>
					</div>
				</div>
			</section>
		<?php endwhile; endif; ?>
	</main>
	<?php
	get_footer();
}
