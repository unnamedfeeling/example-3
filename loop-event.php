<?php
global $options;
$eid=$post->ID;
$ttl=get_the_title();
$prefix='startimecamp_';
$emeta=get_post_meta($eid, '', false);
$ethumb=wp_get_attachment_image_src($emeta['_thumbnail_id'][0], 'small-event-img');
// print_r($emeta);
?>
	<div class="events_ell">
		<div class="events_bg"></div>
		<div class="col-sm-4 col-md-2">
			<?php
			if(!empty($ethumb)){
				// echo $ethumb;
				printf('<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="%s" class="b-lazy" alt="%s">',
					$ethumb[0],
					$ttl
				);
			} else { ?>
				<img src="<?=$options['tpldir']?>/assets/img/events/event-p1.jpg" alt="event-p1">
			<?php } ?>
		</div>
		<div class="col-sm-6 col-md-4 events_pd">
			<p class="events_name"><?=$ttl?></p>
		</div>
		<div class=" col-sm-2 events_pd">
			<p class="events_date"><?php if(!empty($emeta[$prefix.'time'][0])) echo date('d.m.Y', $emeta[$prefix.'time'][0]);?><br>
				<span class="events_time"><?php if(!empty($emeta[$prefix.'time'][0])) echo date('H:i', $emeta[$prefix.'time'][0]);?></span>
			</p>
		</div>
		<div class="col-sm-12 col-md-3 events_pd text-right">
			<div class="btn btn-purple js-event-modal" data-eid="<?=$eid?>">подробнее</div>
		</div>
	</div>
