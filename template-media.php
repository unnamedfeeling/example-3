<?php /* Template Name: Страница медиа */ get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	// $ttl=get_the_title();
	$prefix='startimecamp_';
	// $pmeta=get_post_meta($pid, '', false);
	// $pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	// $bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	// $tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="media" class="js-pad-top">
				<div class="container">
					<div class="media_header text-center">
						<div class="media_h1 h1">
							<label for="s1" class="active" >Фото</label>
							/
							<label for="s2" class="">Видео</label>
						</div>
					</div>
					<div class="switch">
						<input type="radio" name="switch" id="s1" checked />
						<?php
						$args=array(
							'post_type'=>'media',
							// 'posts_per_page'=>-1,
							'posts_per_page'=>12,
							// 'offset'=>7,
							'meta_query'=> array(
								'relation'=>'AND',
								array(
									'key'=>$prefix.'post_type',
									'value'=>'photo'
								)
							),
							'meta_key'=>$prefix.'gallery_date',
							'orderby'=>'meta_value',
							'order'=>'desc'
						);
						$photos=new WP_Query($args);
						// print_r($photos);
						?>
						<div id="photo" class="media_cont" data-page="1" data-total="<?=$photos->post_count?>">
							<?php
							if ($photos->have_posts()){
								while ($photos->have_posts()) : $photos->the_post();
									if($photos->current_post<12){
										get_template_part('loop', 'media_photo');
									}
						 		endwhile;
							} else { ?>
								<div class="noposts">Не найдено ни одного фотоальбома!<br>Добавьте еще фото!</div>
							<?php }	?>
						</div>
						<?php
						if($photos->post_count>='12'){
						?>
						<div class="events_bottom">
							<div class="col-sm-12 col-md-11 text-right">
								<div class="btn btn-green js-get-more-photos">Архив фото</div>
							</div>
						</div>
						<?php }
						wp_reset_query();
						wp_reset_postdata();
						?>
					</div>
					<div class="switch">
						<input type="radio" name="switch" id="s2"  />
						<?php
						$args=array(
							'post_type'=>'media',
							'posts_per_page'=>-1,
							// 'posts_per_page'=>12,
							// 'offset'=>7,
							'meta_query'=> array(
								'relation'=>'AND',
								array(
									'key'=>$prefix.'post_type',
									'value'=>'video'
								)
							),
							// 'meta_key'=>$prefix.'gallery_date',
							// 'orderby'=>'meta_value',
							// 'order'=>'desc'
						);
						$videos=new WP_Query($args);
						// print_r($videos);
						$list1='';
						$list2='';
						if ($videos->have_posts()){
							while ($videos->have_posts()) : $videos->the_post();
								$vmeta=get_post_meta( $post->ID, '', false );
								$poster=(empty($vmeta[$prefix.'posterimg_id'][0])) ? $options['tpldir'].'/assets/files/vposter.jpg' : wp_get_attachment_image_src( $vmeta[$prefix.'posterimg_id'][0], 'vid-poster-img' )[0];
								$video='';
								if(empty($vmeta[$prefix.'yt_id'][0])){
									$video_mp4=(!empty($vmeta[$prefix.'videos_mp4'][0])) ? '<source data-lazy="'.$vmeta[$prefix.'videos_mp4'][0].'" type="video/mp4">' : null;
									$video_webm=(!empty($vmeta[$prefix.'videos_webm'][0])) ? '<source data-lazy="'.$vmeta[$prefix.'videos_webm'][0].'" type="video/webm">' : null;
									$list1.='<div class="slide"><div class="video-cont" data-title="'.$post->post_title.'"><img class="play" src="'.$options["tpldir"].'/assets/img/play.png"><video controls="false" data-poster="'.$poster.'">'.$video_mp4.$video_webm.'Плохой браузер. Без шуток. Технологии просмотра html video уже больше 5 лет, а этот браузер ее не поддерживает. Этого динозавра нужно удалить и поставить <a href="https://www.google.com.ua/chrome/browser/desktop/index.html">браузер</a>. Это ссылка на страницу скачивания chrome. Без спама, капчи или смс.</video></div></div>';
								} else {
									$ytimg='//i.ytimg.com/vi/'.$vmeta[$prefix.'yt_id'][0].'/hqdefault.jpg';
									$video='<div class="slide"><div class="video-cont" data-title="'.$post->post_title.'"><div class="youtube-player" data-id="'.$vmeta[$prefix.'yt_id'][0].'" '.((!empty($vmeta[$prefix.'posterimg_id'][0]))?'data-poster="'.wp_get_attachment_image_src( $vmeta[$prefix.'posterimg_id'][0], 'vid-poster-img' )[0].'"':'data-poster="'.$ytimg.'"').'></div></div></div>';
									$list1.=$video;
								}
								$list2.='<div class="slide"><img data-lazy="'.$poster.'" alt=""></div>';
							endwhile;
						}
						?>
						<div id="video" class="media_cont">
							<div class="row">
								<div class="col-xs-12 col-sm-10 col-sm-offset-1">
									<div id="media-slick">
										<?=$list1?>
									</div>
								</div>
								<div class="col-xs-12">
									<div id="media-nav-slick">
										<?=$list2?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php endwhile; ?>

		<?php else: ?>
			<section>
				<article>
					<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
				</article>
			</section>
		<?php endif; ?>
	</main>

<?php get_footer(); ?>
