<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(); global $options; ?></title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-57x57.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-60x60.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-72x72.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-76x76.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-114x114.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-120x120.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-144x144.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-152x152.png?v=Ufjmas3">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=$options['tpldir']?>/assets/img/favicon/apple-touch-icon-180x180.png?v=Ufjmas3">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=$options['tpldir']?>/assets/img/favicon/favicon.png?v=Ufjmas3">
		<link rel="icon" type="image/png" sizes="194x194" href="<?=$options['tpldir']?>/assets/img/favicon/favicon-194x194.png?v=Ufjmas3">
		<link rel="icon" type="image/png" sizes="192x192" href="<?=$options['tpldir']?>/assets/img/favicon/android-chrome-192x192.png?v=Ufjmas3">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=$options['tpldir']?>/assets/img/favicon/favicon.png?v=Ufjmas3">
		<link rel="manifest" href="<?=$options['tpldir']?>/assets/img/favicon/manifest.json?v=Ufjmas3">
		<link rel="mask-icon" href="<?=$options['tpldir']?>/assets/img/favicon/safari-pinned-tab.svg?v=Ufjmas3" color="#5bbad5">
		<!-- <link rel="shortcut icon" href="<?=$options['tpldir']?>/assets/img/favicon/favicon.ico?v=Ufjmas3"> -->
		<meta name="msapplication-TileColor" content="#00aba9">
		<meta name="msapplication-TileImage" content="<?=$options['tpldir']?>/assets/img/favicon/mstile-144x144.png?v=Ufjmas3">
		<meta name="theme-color" content="#d9ebf0">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head(); ?>
		<!--[if lt IE 9]><script src="<?=$options['tpldir']?>/assets/js/html5.js"></script><![endif]-->
	</head>
	<body <?php body_class('loaded'); ?>>
		<div class="main-wrapper">
