<?php
/**
 * Author: Todd Motto | @toddmotto
 * URL: startimecamp.com | @startimecamp
 * Custom functions, support, custom post types and more.
 */

// require_once "modules/is-debug.php";
$prefix='startimecamp_';

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// CMB2
if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/cmb2/init.php';
} elseif ( file_exists(  dirname( __FILE__ ) . '/assets/php/CMB2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/CMB2/init.php';
}
// CMB2 attached posts
if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php';
}
// CMB2 rgba colorpicker
// if ( file_exists( dirname( __FILE__ ) . '/assets/php/CMB2_RGBa_Picker/jw-cmb2-rgba-colorpicker.php' ) ) {
//   require_once dirname( __FILE__ ) . '/assets/php/CMB2_RGBa_Picker/jw-cmb2-rgba-colorpicker.php';
// }

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
	$content_width = 900;
}

if (function_exists('add_theme_support'))
{

	// Add Thumbnail Theme Support
	add_theme_support('post-thumbnails');
	add_image_size('large', 700, '', true); // Large Thumbnail
	add_image_size('medium', 250, '', true); // Medium Thumbnail
	add_image_size('small', 120, '', true); // Small Thumbnail
	add_image_size('small-event-img', 140, 135, true);
	add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
	add_image_size('teacherimg', 156, 156, true);
	add_image_size('shift-photo-gal', 768, 511, true);
	add_image_size('shift-mainimg-loop', 376, 435, true);
	add_image_size('main-living-img', 738, 432, true);
	add_image_size('about-slider-img', 640, 426, true);
	add_image_size('event-img', 979, 375, false);
	// add_image_size('media-modal-img', 1140, 760, true);
	// add_image_size('media-modal-img', 9999, 760, false);
	add_image_size('media-modal-img', 1140, 760, false);
	add_image_size('photo-loop-img', 230, 230, true);
	add_image_size('photo-single-loop-img', 230, 999, false);
	add_image_size('vid-poster-img', 946, 532, true);

	// Add Support for Custom Backgrounds - Uncomment below if you're going to use
	/*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
	));*/

	// Add Support for Custom Header - Uncomment below if you're going to use
	/*add_theme_support('custom-header', array(
	'default-image'		  => get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'	 => '000',
	'width'				  => 1000,
	'height'				 => 198,
	'random-default'		 => false,
	'wp-head-callback'	   => $wphead_cb,
	'admin-head-callback'	=> $adminhead_cb,
	'admin-preview-callback' => $adminpreview_cb
	));*/

	// Enables post and comment RSS feed links to head
	add_theme_support('automatic-feed-links');

	// Enable HTML5 support
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

	// Localisation Support
	load_theme_textdomain('startimecamp', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Global options Define
function startimecamp_globals(){
	global $options;
	$options['brnc']=get_option('startimecamp_options');
	$options['gnrl']=get_option('general_options');
	$options['docs']=get_option('docs_options');
	$options['socl']=get_option('social_options');
	$options['advd']=get_option('advanced_options');
	$options['tpldir']=get_template_directory_uri();
	$options['translate'] = array(
		"am" => "дп",
		"pm" => "пп",
		"AM" => "ДП",
		"PM" => "ПП",
		"Monday" => "Понедельник",
		"Mon" => "пн",
		"Tuesday" => "Вторник",
		"Tue" => "вт",
		"Wednesday" => "Среда",
		"Wed" => "ср",
		"Thursday" => "Четверг",
		"Thu" => "чт",
		"Friday" => "Пятница",
		"Fri" => "пт",
		"Saturday" => "Суббота",
		"Sat" => "сб",
		"Sunday" => "Воскресенье",
		"Sun" => "вс",
		"January" => "Января",
		"Jan" => "янв",
		"February" => "Февраля",
		"Feb" => "фев",
		"March" => "Марта",
		"Mar" => "мар",
		"April" => "Апреля",
		"Apr" => "апр",
		"May" => "мая",
		"June" => "Июня",
		"Jun" => "июн",
		"July" => "Июля",
		"Jul" => "июл",
		"August" => "Августа",
		"Aug" => "авг",
		"September" => "Сентября",
		"Sep" => "сен",
		"October" => "Октября",
		"Oct" => "окт",
		"November" => "Ноября",
		"Nov" => "ноя",
		"December" => "Декабря",
		"Dec" => "дек",
		"st" => "ое",
		"nd" => "ое",
		"rd" => "е",
		"th" => "ое"
	);
}

// HTML5 Blank navigation
function startimecamp_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'			=> '',
		'container'	   => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'	=> '',
		'menu_class'	  => 'menu',
		'menu_id'		 => '',
		'echo'			=> true,
		'fallback_cb'	 => 'wp_page_menu',
		'before'		  => '',
		'after'		   => '',
		'link_before'	 => '',
		'link_after'	  => '',
		'items_wrap'	  => '<ul class="header__bottom-menu-list">%3$s</ul>',
		'depth'		   => 0,
		// 'walker'		  => new Nav_Menu_Walker
		'walker'		  => ''
		)
	);
}

// Menu walker
class Nav_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth	 Depth of menu item. May be used for padding.
	 * @param  array $args	Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		$class=($depth>=1) ? 'header__bottom-sub-menu-item' : 'header__bottom-menu-item';
		$class.=($depth==0&&$item->current) ? ' active':'';
		// $class.=($depth>=1 ? 'header__bottom-sub-menu-link ' : 'header__bottom-menu-link ');
		$class.=($depth>=1&&$item->current ? 'active ':'');
		$class.=' d-'.$depth;
		$output.= '<li class="'.$class.'">';
		$jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-drop-btn' : '';
		$attributes  = ($depth>=1) ? ' class="header__bottom-sub-menu-link"' : ' class="header__bottom-menu-link'.$jsbtn.'"';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title	   = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title	   = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<div class="js-sub-menu header__bottom-sub-menu"><ul class="header__bottom-sub-menu-list">';
		} else {
			$output .= '<ul class="header__bottom-menu-list">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		// $output .= '</ul>';
		if($depth>=0){
			$output .= '</ul></div>';
		} else {
			$output .= '</li>';
		}
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

function startimecamp_footer_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'footer-menu',
		'menu'			=> '',
		'container'	   => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'	=> '',
		'menu_class'	  => 'menu',
		'menu_id'		 => '',
		'echo'			=> true,
		'fallback_cb'	 => 'wp_page_menu',
		'before'		  => '',
		'after'		   => '',
		'link_before'	 => '',
		'link_after'	  => '',
		'items_wrap'	  => '<ul class="footer-main-list clearfix">%3$s</ul>',
		'depth'		   => 0,
		// 'walker'		  => new Nav_Footer_Menu_Walker
		'walker'		  => ''
		)
	);
}

// Menu walker
class Nav_Footer_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth	 Depth of menu item. May be used for padding.
	 * @param  array $args	Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		$jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-footer-submenu' : '';
		$class='footer-main__item'.$jsbtn;
		$class.=($depth==0&&$item->current) ? ' active':'';
		$class.=($depth>=1 ? 'footer-main__link ':'');
		$class.=($depth>=1&&$item->current ? 'active ':'');
		$class.=' d-'.$depth;
		$output.= '<li class="'.$class.'">';
		$attributes  = ($depth>=1) ? '' : ' class="footer-main__link"';
		// $attributes  = '';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title	   = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title	   = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="footer-main-sublist">';
		} else {
			$output .= '<ul class="footer-main-list clearfix">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		$output .= '</ul>';
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

// Load HTML5 Blank scripts (header.php)
function startimecamp_header_scripts()
{
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );
		wp_deregister_script( 'imagesloaded' );
		wp_enqueue_script('jquery', '//cdn.jsdelivr.net/g/jquery@3.2.1,jquery.migrate@1.4.1,blazy@1.8.2', array(), '2.2.4');
		wp_enqueue_script('slickslider', get_template_directory_uri() . '/assets/libs/slick/slick.min.js', array('jquery'), '1.6.0');
	}
}

// Load HTML5 Blank conditional scripts
function startimecamp_conditional_scripts()
{
	if (is_page('pagenamehere')) {
		// Conditional script(s)
		wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0');
		wp_enqueue_script('scriptname');
	}
}

// Load HTML5 Blank styles
function startimecamp_styles()
{
	// wp_enqueue_style('themestyles-ext', '//cdn.jsdelivr.net/g/jquery.slick@1.6.0(slick.css),animatecss@3.5.2,jquery.magnific-popup@1.0.0(magnific-popup.css)', array(), '1.0', 'all');
	// wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '1.0', 'all');
	wp_enqueue_style('themestyles-ext', '//cdn.jsdelivr.net/g/jquery.slick@1.6.0(slick.css)', array(), '1.0', 'all');
	wp_enqueue_style('mainstyle', get_template_directory_uri() . '/assets/css/style.min.css', array(), '1.0', 'all');
}

// footer scripts
function startimecamp_footer_scripts(){
	global $options;
	wp_deregister_script( 'wp-embed' );
	wp_enqueue_script('startimescripts-min', get_template_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), '1.0.0');
	wp_localize_script( 'startimescripts-min', 'ajax_func', array(
		'nonce'	=> wp_create_nonce( 'ultimate-nonce' ),
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
	if(!empty($options['socl']['address_camp_map'])){
		$addr=explode(', ', $options['socl']['address_camp_map']);
		wp_localize_script( 'startimescripts-min', 'additionalData', array(
			'address_camp_map'	=> array(
				'lat' => (float)$addr[0],
				'lng' => (float)$addr[1]
			),
		));
	}
	wp_localize_script( 'startimescripts-min', 'servdata', array(
		'tpldir'	=> get_template_directory_uri(),
	));
}

//cleanup version tag
function remove_cssjs_ver( $src ) {
	$src = remove_query_arg( array('v', 'ver', 'rev', 'id'), $src );
	return $src;
}

// remove emoji
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  // add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
// Register HTML5 Blank Navigation
function register_html5_menu()
{
	register_nav_menus(array( // Using array to specify more menus if needed
		'header-menu' => __('Главное меню', 'startimecamp'), // Main Navigation
		'footer-menu' => __('Меню в подвале', 'startimecamp'), // Sidebar Navigation
		// 'extra-menu' => __('Extra Menu', 'startimecamp') // Extra Navigation if needed (duplicate as many as you need!)
	));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
	$args['container'] = false;
	return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
	return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
	return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}

// Remove the width and height attributes from inserted images
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
	// Define Sidebar Widget Area 1
	register_sidebar(array(
		'name' => __('Widget Area 1', 'startimecamp'),
		'description' => __('Description for this widget-area...', 'startimecamp'),
		'id' => 'widget-area-1',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

	// Define Sidebar Widget Area 2
	register_sidebar(array(
		'name' => __('Widget Area 2', 'startimecamp'),
		'description' => __('Description for this widget-area...', 'startimecamp'),
		'id' => 'widget-area-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
	global $wp_widget_factory;

	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action('wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		));
	}
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
	global $wp_query;
	$big = 999999999;
	echo paginate_links(array(
		'base' => str_replace($big, '%#%', get_pagenum_link($big)),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'total' => $wp_query->max_num_pages
	));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
	return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
	return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
	global $post;
	if (function_exists($length_callback)) {
		add_filter('excerpt_length', $length_callback);
	}
	if (function_exists($more_callback)) {
		add_filter('excerpt_more', $more_callback);
	}
	$output = get_the_excerpt();
	$output = apply_filters('wptexturize', $output);
	$output = apply_filters('convert_chars', $output);
	$output = '<p>' . $output . '</p>';
	echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
	global $post;
	return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'startimecamp') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
	return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}

// Custom Gravatar in Settings > Discussion
function startimecampgravatar ($avatar_defaults)
{
	$myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[$myavatar] = "Custom Gravatar";
	return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script('comment-reply');
		}
	}
}

// Custom Comments Callback
function startimecampcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
	<!-- heads up: starting < for the html tag (li or div) in the next line: -->
	<<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

function unfilter_html_tags( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization.
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	$value = strip_tags( $value, '<p><a><br><br/><span>' );

	return $value;
}
function unfilter_all( $value, $field_args, $field ) {
	$value=str_replace('\\\"', '"', $value);
	return $value;
}

// ajax get template modal contents
function get_evt_modal_cont(){
	global $options;
	// $params=$_POST['params'];
	$e=get_post( $_POST['params']['id'], OBJECT, 'raw' );
	$eid=$e->ID;
	$ettl=get_the_title($eid);
	$prefix='startimecamp_';
	$emeta=get_post_meta($eid, '', false);
	$ethumb=(!empty($emeta[$prefix.'bigimg_id'][0])) ? wp_get_attachment_image($emeta[$prefix.'bigimg_id'][0], 'event-img', false, array('alt'=>$ettl)) : wp_get_attachment_image($emeta['_thumbnail_id'][0], 'event-img', false, array('alt'=>$ettl));
	$form=(!empty($emeta[$prefix.'form'][0])) ? $emeta[$prefix.'form'][0] : '[contact-form-7 title="Подать заявку" html_class="form form-feedback"]';
	// die(json_encode($e));
	ob_start();
	?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 medpop_cont medpop_cont-1 text-center">
				<p class="medpop_date">
					<?php if(!empty($emeta[$prefix.'time'][0])) echo date('d.m.Y', $emeta[$prefix.'time'][0]);?>
					<span class="time"><?php if(!empty($emeta[$prefix.'time'][0])) echo date('H:i', $emeta[$prefix.'time'][0]);?></span>
				</p>
			</div>
			<div class="col-xs-12 col-sm-9 medpop_cont">
				<div class="medpop_top">
					<p class="medpop_beg">Предстоящие события</p>
					<div class="medpop_btn medpop_btn-close icon-mod_close">
					</div>
				</div>
				<div class="medpop_h1"><?php echo $ettl ?></div>
				<div class="medpop_poster">
					<?php
					if(!empty($ethumb)) { echo $ethumb; } else {
					?>
					<img src="<?=$options['tpldir']?>/assets/img/pop-poster.jpg" class="" alt="medpop_poster">
					<?php } ?>
					<?php //<div class="btn btn-purple medpop_btn-reg js-medpop_btn-reg">Подробнее</div> ?>
				</div>
				<div class="medpop_discr">
					<?php echo apply_filters('the_content', $e->post_content); ?>
				</div>
			</div>
		</div>
	</div>
	<div id="evt-register" class="popup">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
					<div class="h"></div>
					<div class="form-cont">
						<?=do_shortcode($form)?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	$cont=ob_get_clean();
	die(json_encode($cont));
}

// ajax get template modal contents
function get_evts(){
	global $options;
	// wp_reset_query();
	// die(json_encode( $_POST ));
	$params=$_POST;
	$offset=$params['params']['p']*6;
	$args=array(
		'post_type'=>'event',
		'posts_per_page'=>6,
		'offset'=>$offset,
		'meta_key'=>'startimecamp_time',
		'orderby'=>'meta_value',
		'order'=>'asc'
	);
	$events=new WP_Query($args);
	ob_start();
	if ($events->have_posts()): while ($events->have_posts()) : $events->the_post();
		get_template_part('loop', 'event');
	endwhile;
	else : ?>
		<div class="noposts">Больше не найдено ни одного события!<br>Добавьте события!</div>
	<?php endif;
	// $response['cont']=ob_get_clean();
	$response['offset']=$offset;
	$response['total_posts']=wp_count_posts('event');
	$response['content']=ob_get_clean();
	die(json_encode($response));
}

// ajax get template modal contents
function get_photos(){
	global $options, $post;
	$prefix='startimecamp_';
	$params=$_POST;
	$total=$params['params']['t'];
	$offset=$params['params']['p']*12;
	$aj_args=array(
		'post_type'=>'media',
		// 'posts_per_page'=>-1,
		'posts_per_page'=>12,
		'offset'=>$offset,
		'meta_query'=> array(
			'relation'=>'AND',
			array(
				'key'=>$prefix.'post_type',
				'value'=>'photo'
			)
		),
		'meta_key'=>$prefix.'gallery_date',
		'orderby'=>'meta_value',
		'order'=>'desc'
	);
	$aj_photos=new WP_Query($aj_args);
	ob_start();
	if ($aj_photos->have_posts()): while ($aj_photos->have_posts()) : $aj_photos->the_post();
		get_template_part('loop', 'media_photo');
	endwhile;
	else : ?>
		<div class="noposts">Больше не найдено ни одного фотоальбома!<br>Добавьте еще фото!</div>
	<?php endif;
	$response['content']=ob_get_clean();
	$response['offset']=$offset;
	$response['total_posts']=$total;
	die(json_encode($response));
}

// ajax get template modal contents
function get_media_modal(){
	global $options;
	$prefix='startimecamp_';
	$params=$_POST;
	$imgs=explode(',', $params['params']['i']);
	// die($imgs);
	ob_start();
	// $arr=explode(',', $imgs);
	// print_r($arr);
	echo '<div class="slick-pop"><div class="container"><div class="h"></div><div class="scont"><div class="scont_btn-close icon-mod_close"></div><div id="media-slick-2">';
	foreach ($imgs as $key => $val) {
		$src=wp_get_attachment_image_src( $val, 'media-modal-img' );
		printf('<div class="slide"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="%s"></div>',
			$src[0]
		);
	}
	echo '</div></div></div></div>';
	$response['content']=ob_get_clean();
	die(json_encode($response));
}


// set custom number for 'posts_per_page' per posttype
function posts_per_page_func( $query ) {
	$prefix='startimecamp_';
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'event' ) ) {
		$query->set( 'posts_per_page', '6' );
		$query->set( 'meta_key', 'startimecamp_time' );
		$query->set( 'orderby', 'meta_value' );
		$query->set( 'order', 'asc' );
	}
}

// integrate amocrm for any wpcf form
function amocrm_integrate(){
	$params=$_POST['params'];
	// die(json_encode( $params ));
	$crmUS_data = array();
	$crmUS_data['ga'] = rand(0,9999999999); //cid_ar
	// $crmUS_data['name'] = $params[0]['value'];
	// $crmUS_data['phone'] =$params[1]['value'];
	// $crmUS_data['email'] = '';
	$usname=$phone=$email='';
	foreach ($params as $val) {
		$usname=($val['name']=='your-name')?$val['value']:$usname;
		$phone=($val['name']=='your-phone')?$val['value']:$phone;
		$email=($val['name']=='your-email')?$val['value']:$email;
		$msg=($val['name']=='textarea-main')?$val['value']:$msg;
		// $resp[]=$val['value'];
	}
	// die(json_encode($usname));
	// die(json_encode( $phone ));
	$crmUS_data['name'] = $usname;
	$crmUS_data['phone'] = $phone;
	$crmUS_data['email'] = $email;
	$crmUS_data['comment'] = 'Заявка создана с сайта startimecamp.com '.($msg ? ' Сообщение: ' . $msg : '');
	$crmUS_data['href'] = $_SERVER['HTTP_REFERER'];
	$crmUS_data['hash'] = "671431e6bb815fe21ca8c5bf31eeee58";
	$crmUS_data['RAND'] = "-1";
	$crmUS_data['source'] = "order";
	$crmUS_data['name_lead'] = "Заявка StarTime Camp - ".$usname;
	$crmUS_data['need_response'] = false;
	// die(json_encode( $crmUS_data ));
	$crmUS_Curl = curl_init();
	curl_setopt_array($crmUS_Curl, array(
		CURLOPT_URL => 'http://stable.in.ua/crmUs/lab2/engine/amo.php',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => http_build_query($crmUS_data)
	));
	curl_exec($crmUS_Curl);
	curl_close($crmUS_Curl);
}

// send data to amo
function amo_connect($wpcf7){
	if ( file_exists( dirname( __FILE__ ) . '/assets/php/amosend.php' ) ) {
		require_once dirname( __FILE__ ) . '/assets/php/amosend.php';
	}
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('cmb2_admin_init', 'startimecamp_metaboxes' );
add_action('wp_enqueue_scripts', 'startimecamp_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'startimecamp_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'startimecamp_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('init', 'startimecamp_globals');
add_action('init', 'disable_wp_emojicons');
add_action('wp_footer', 'startimecamp_footer_scripts');
add_action( 'wp_ajax_nopriv_modal_cont', 'get_evt_modal_cont' );
add_action( 'wp_ajax_modal_cont', 'get_evt_modal_cont' );
add_action( 'wp_ajax_nopriv_get_events', 'get_evts' );
add_action( 'wp_ajax_get_events', 'get_evts' );
add_action( 'wp_ajax_nopriv_get_photos', 'get_photos' );
add_action( 'wp_ajax_get_photos', 'get_photos' );
add_action( 'wp_ajax_nopriv_get_media_modal', 'get_media_modal' );
add_action( 'wp_ajax_get_media_modal', 'get_media_modal' );
add_action( 'wp_ajax_nopriv_startime_amocrm_integrate', 'amocrm_integrate' );
add_action( 'wp_ajax_startime_amocrm_integrate', 'amocrm_integrate' );
add_action( 'pre_get_posts', 'posts_per_page_func' );
add_action('wpcf7_before_send_mail', 'amo_connect');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'startimecampgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
// add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('post_thumbnail_html', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter('image_send_to_editor', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter( 'script_loader_src', 'remove_cssjs_ver', 15, 1 );
add_filter( 'style_loader_src', 'remove_cssjs_ver', 15, 1 );

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
// add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
// add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
	if ( file_exists( dirname( __FILE__ ) . '/assets/php/post-types.php' ) ) {
		require_once dirname( __FILE__ ) . '/assets/php/post-types.php';
	}
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
	return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
	return '<h2>' . $content . '</h2>';
}

/*------------------------------------*\
	Metabox Functions
\*------------------------------------*/

function startimecamp_metaboxes() {
	if ( file_exists( dirname( __FILE__ ) . '/assets/php/metaboxes.php' ) ) {
		require_once dirname( __FILE__ ) . '/assets/php/metaboxes.php';
	}
}

/**
 * CMB Tabbed Theme Options
 *
 * @author	Arushad Ahmed <@dash8x, contact@arushad.org>
 * @link	  http://arushad.org/how-to-create-a-tabbed-options-page-for-your-wordpress-theme-using-cmb
 * @version   0.1.0
 */
class startimecampMain_Admin {

	/**
	 * Default Option key
	 * @var string
	 */
	private $key = 'startimecamp_options_global';

	/**
	 * Array of metaboxes/fields
	 * @var array
	 */
	protected $option_metabox = array();

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Tab Pages
	 * @var array
	 */
	protected $options_pages = array();

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Глобальные настройки', 'startimecamp' );
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) ); //create tab pages
	}

	/**
	 * Register our setting tabs to WP
	 * @since  0.1.0
	 */
	public function init() {
		$option_tabs = self::option_fields();
		foreach ($option_tabs as $index => $option_tab) {
			register_setting( $option_tab['id'], $option_tab['id'] );
		}
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$option_tabs = self::option_fields();
		foreach ($option_tabs as $index => $option_tab) {
			if ( $index == 0) {
				$this->options_pages[] = add_menu_page( $this->title, $this->title, 'manage_options', $option_tab['id'], array( $this, 'admin_page_display' ) ); //Link admin menu to first tab
				add_submenu_page( $option_tabs[0]['id'], $this->title, $option_tab['title'], 'manage_options', $option_tab['id'], array( $this, 'admin_page_display' ) ); //Duplicate menu link for first submenu page
			} else {
				$this->options_pages[] = add_submenu_page( $option_tabs[0]['id'], $this->title, $option_tab['title'], 'manage_options', $option_tab['id'], array( $this, 'admin_page_display' ) );
			}
		}
	}

	/**
	 * Admin page markup. Mostly handled by CMB
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		$option_tabs = self::option_fields(); //get all option tabs
		$tab_forms = array();
		?>
		<div class="wrap cmb_options_page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

			<!-- Options Page Nav Tabs -->
			<h2 class="nav-tab-wrapper">
				<?php foreach ($option_tabs as $option_tab) :
					$tab_slug = $option_tab['id'];
					$nav_class = 'nav-tab';
					if ( $tab_slug == $_GET['page'] ) {
						$nav_class .= ' nav-tab-active'; //add active class to current tab
						$tab_forms[] = $option_tab; //add current tab to forms to be rendered
					}
				?>
				<a class="<?php echo $nav_class; ?>" href="<?php menu_page_url( $tab_slug ); ?>"><?php esc_attr_e($option_tab['title']); ?></a>
				<?php endforeach; ?>
			</h2>
			<!-- End of Nav Tabs -->

			<?php foreach ($tab_forms as $tab_form) : //render all tab forms (normaly just 1 form) ?>
			<div id="<?php esc_attr_e($tab_form['id']); ?>" class="group">
				<?php cmb2_metabox_form( $tab_form, $tab_form['id'] ); ?>
			</div>
			<?php endforeach; ?>
		</div>
		<?php
	}

	/**
	 * Defines the theme option metabox and field configuration
	 * @since  0.1.0
	 * @return array
	 */
	public function option_fields() {

		// $prefix='startimecamp_';

		// Only need to initiate the array once per page-load
		if ( ! empty( $this->option_metabox ) ) {
			return $this->option_metabox;
		}

		$this->option_metabox[] = array(
			'id'		 => 'general_options', //id used as tab page slug, must be unique
			'title'	  => 'Общие',
			'show_on'	=> array( 'key' => 'options-page', 'value' => array( 'general_options' ), ), //value must be same as id
			'show_names' => true,
			'fields'	 => array(
				array(
					'name' => __('Главный логотип', 'startime'),
					'desc' => __('выбрать файл для лого на всех страницах', 'startime'),
					'id' => 'header_logo', //each field id must be unique
					'default' => ' ',
					'type' => 'file',
					'query_args' => array(
						'type' => array('image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff')
					)
				),
				array(
					'name' => __('Заголовок архива событий', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'evt_title', //each field id must be unique
					'default' => '',
					'type' => 'text',
				),
				// array(
				// 	'name' => __('Время работы', 'startimecamp'),
				// 	// 'desc' => __('Сюда пишем адрес', 'startimecamp'),
				// 	'id'   => 'general_options_title0',
				// 	'type' => 'title'
				// ),
				// array(
				// 	'name' => __('Время работы в будние дни', 'startimecamp'),
				// 	// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
				// 	'id' => 'weekdays_hours', //each field id must be unique
				// 	'default' => '',
				// 	'type' => 'text',
				// ),
				// array(
				// 	'name' => __('Время работы в выходные дни', 'startimecamp'),
				// 	// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
				// 	'id' => 'weekends_hours', //each field id must be unique
				// 	'default' => '',
				// 	'type' => 'text',
				// ),
			)
		);

		$this->option_metabox[] = array(
			'id'		 => 'docs_options', //id used as tab page slug, must be unique
			'title'	  => 'Документы',
			'show_on'	=> array( 'key' => 'options-page', 'value' => array( 'docs_options' ), ), //value must be same as id
			'show_names' => true,
			'fields'	 => array(
				array(
					'name' => __('Документы', 'startimecamp'),
					// 'desc' => __('Сюда пишем адрес', 'startimecamp'),
					'id'   => 'docs_options_title0',
					'type' => 'title'
				),
				array(
					'name' => __('Прайс-лист общий', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-price', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Анкета общая', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-table', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Информация для родителей', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-info', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Правила лагеря', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-rules', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Что взять с собой', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-wares', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 1', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl1', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 1', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs1', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 2', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl2', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 2', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs2', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 3', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl3', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 3', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs3', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 4', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl4', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 4', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs4', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 5', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl5', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 5', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs5', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 6', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl6', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 6', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs6', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 7', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl7', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 7', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs7', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 8', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl8', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 8', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs8', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 9', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl9', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 9', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs9', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
				array(
					'name' => __('Наименование дополнительного документа 10', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs-ttl10', //each field id must be unique
					// 'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Дополнительный документ 10', 'startimecamp'),
					// 'desc' => __('выбрать файл для лого на всех страницах', 'startimecamp'),
					'id' => 'main-docs10', //each field id must be unique
					// 'default' => '',
					'type' => 'file',
					'query_args' => array(
						'type' => 'application/pdf', // Make library only display PDFs.
					),
				),
			)
		);

		$this->option_metabox[] = array(
			'id'		 => 'social_options',
			'title'	  => 'Адрес, Соцсети, Email',
			'show_on'	=> array( 'key' => 'options-page', 'value' => array( $prefix.'social_options' ), ),
			'show_names' => true,
			'fields'	 => array(
				array(
					'name' => __('Адреса', 'startimecamp'),
					'desc' => __('Сюда пишем адрес', 'startimecamp'),
					'id'   => 'social_options_title0',
					'type' => 'title'
				),
				array(
					'name' => __('Адрес отдела продаж', 'startimecamp'),
					// 'desc' => __('Сюда пишем ссылки на соцсети', 'startimecamp'),
					'id'   => 'address',
					'type' => 'text',
					// 'sanitization_cb' => 'unfilter_html_tags'
				),
				array(
					'name' => __('Адрес лагеря', 'startimecamp'),
					// 'desc' => __('Сюда пишем ссылки на соцсети', 'startimecamp'),
					'id'   => 'address_camp',
					'type' => 'text',
					// 'sanitization_cb' => 'unfilter_html_tags'
				),
				array(
					'name' => __('Лагерь на карте', 'startimecamp'),
					'desc' => __('Сюда пишем положение лагеря и google maps - широту и долготу', 'startimecamp'),
					'id'   => 'address_camp_map',
					'type' => 'text'
				),
				array(
					'name' => __('Телефоны (контакты)', 'startimecamp'),
					'desc' => __('Данные для глобальных настроек контактных данных', 'startimecamp'),
					'id'   => 'social_options_title1',
					'type' => 'title'
				),
				// array(
				// 	'name' => __('Горячая линия', 'startimecamp'),
				// 	'desc' => __('вставить телефон для вывода в шапке в разделе "горячая линия"', 'startimecamp'),
				// 	'id' => 'hotline',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
				array(
					'name' => __('Телефон отдела продаж', 'startimecamp'),
					'desc' => __('выводятся в хедере; указать ОДИН телефон в виде html', 'startimecamp'),
					'id' => 'header_phone',
					'default' => '',
					'type' => 'text',
					'sanitization_cb' => 'unfilter_html_tags'
				),
				array(
					'name' => __('Телефоны отдела продаж', 'startimecamp'),
					'desc' => __('выводятся справа от расписания в футере; указать через запятую БЕЗ ПРОБЕЛА', 'startimecamp'),
					'id' => 'footer_phones',
					'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Телефоны лагеря', 'startimecamp'),
					'desc' => __('выводятся справа от расписания в футере; указать через запятую БЕЗ ПРОБЕЛА', 'startimecamp'),
					'id' => 'footer_phones_camp',
					'default' => '',
					'type' => 'text'
				),
				// array(
				// 	'name' => __('Телефон для футера 1 (главный)', 'startimecamp'),
				// 	// 'desc' => __('выводятся справа от расписания в футере; указать через запятую БЕЗ ПРОБЕЛА', 'startimecamp'),
				// 	'id' => 'phone1',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
				// array(
				// 	'name' => __('Телефон для футера 2', 'startimecamp'),
				// 	// 'desc' => __('выводятся справа от расписания в футере; указать через запятую БЕЗ ПРОБЕЛА', 'startimecamp'),
				// 	'id' => 'phone2',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
				// array(
				// 	'name' => __('Телефон для футера 3', 'startimecamp'),
				// 	// 'desc' => __('выводятся справа от расписания в футере; указать через запятую БЕЗ ПРОБЕЛА', 'startimecamp'),
				// 	'id' => 'phone3',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
				array(
					'name' => __('Соцсети', 'startimecamp'),
					'desc' => __('Сюда пишем ссылки на соцсети', 'startimecamp'),
					'id'   => 'social_options_title3',
					'type' => 'title'
				),
				array(
					'name' => __('Instagram', 'startimecamp'),
					'desc' => __('вставить ссылку на страницу', 'startimecamp'),
					'id' => 'instagram',
					'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('VK.com', 'startimecamp'),
					'desc' => __('вставить ссылку на страницу', 'startimecamp'),
					'id' => 'vk',
					'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Facebook', 'startimecamp'),
					'desc' => __('вставить ссылку на страницу', 'startimecamp'),
					'id' => 'facebook',
					'default' => '',
					'type' => 'text'
				),
				// array(
				// 	'name' => __('Youtube', 'startimecamp'),
				// 	'desc' => __('вставить ссылку на страницу', 'startimecamp'),
				// 	'id' => 'youtube',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
				array(
					'name' => __('Email', 'startimecamp'),
					'desc' => __('Сюда пишем разные email', 'startimecamp'),
					'id'   => 'social_options_title2',
					'type' => 'title'
				),
				array(
					'name' => __('Email', 'startimecamp'),
					'desc' => __('вставить email', 'startimecamp'),
					'id' => 'email',
					'default' => '',
					'type' => 'text'
				),
				array(
					'name' => __('Формы', 'startimecamp'),
					// 'desc' => __('Сюда пишем разные email', 'startimecamp'),
					'id'   => 'social_options_title4',
					'type' => 'title'
				),
				array(
					'name' => __('Шорткод формы для подписки', 'startimecamp'),
					'desc' => __('вставить шорткод', 'startimecamp'),
					'id' => 'subscr-form',
					'default' => '',
					'type' => 'text'
				),
				// array(
				// 	'name' => __('Шорткод формы для модального окна', 'startimecamp'),
				// 	'desc' => __('вставить шорткод', 'startimecamp'),
				// 	'id' => 'contact-form',
				// 	'default' => '',
				// 	'type' => 'text'
				// ),
			)
		);

		$this->option_metabox[] = array(
			'id'		 => 'advanced_options',
			'title'	  => 'Другие настройки',
			'show_on'	=> array( 'key' => 'options-page', 'value' => array( 'advanced_options' ), ),
			'show_names' => true,
			'fields'	 => array(
				array(
					'name' => __('Особый CSS', 'startimecamp'),
					'desc' => __('Сюда вносим кастомный css.<br><b>Теги <code>&lt;style&gt; и &lt;/style&gt;</code> опускаем - сюда пишем ТОЛЬКО ЧИСТЫЙ CSS</b>', 'startimecamp'),
					'id' => 'custom_css',
					'default' => ' ',
					'type' => 'textarea',
					'escape_cb' => false
				),
				array(
					'name' => __('Особый JS', 'startimecamp'),
					'desc' => __('Пиксели, метрики и другие важные js-вещи. Будет выводиться где надо и как надо.<br><b>Теги <code>&lt;script&gt; и &lt;/script&gt;</code> опускаем - сюда пишем ТОЛЬКО ЧИСТЫЙ JS</b>', 'startimecamp'),
					'id' => 'custom_js',
					'default' => ' ',
					'type' => 'textarea',
					'escape_cb' => false
				),
				array(
					'name' => __('Особый JS, который нужно выводить с тегом "script"', 'startimecamp'),
					'desc' => __('Пиксели, метрики и другие важные js-вещи. Будет выводиться где надо и как надо.<br><b>Теги <code>&lt;script&gt; и &lt;/script&gt;</code> указываем тут - это нужно для тех случаев, когда нужно задать код, который подключает сторонний файл напрямую</b>. НУЖНО ДОБАВЛЯТЬ АТРИБУТ <b>async="async"</b> К ТАКОМУ КОДУ ОБЯЗАТЕЛЬНО', 'startimecamp'),
					'id' => 'custom_js_tag',
					'default' => '',
					'type' => 'textarea',
					'sanitization_cb' => 'unfilter_all'
				),
				array(
					'name' => __('Особый noscript', 'startimecamp'),
					'desc' => __('Если к коду идет дополнение в тегах noscript.<br><b>Теги <code>&lt;noscript&gt; и &lt;/noscript&gt;</code> опускаем</b>', 'startimecamp'),
					'id' => 'custom_noscript',
					'default' => ' ',
					'type' => 'textarea',
					'sanitization_cb' => 'unfilter_all'
				),
			)
		);

		//insert extra tabs here

		return $this->option_metabox;
	}

	/**
	 * Returns the option key for a given field id
	 * @since  0.1.0
	 * @return array
	 */
	public function get_option_key($field_id) {
		$option_tabs = $this->option_fields();
		foreach ($option_tabs as $option_tab) { //search all tabs
			foreach ($option_tab['fields'] as $field) { //search all fields
				if ($field['id'] == $field_id) {
					return $option_tab['id'];
				}
			}
		}
		return $this->key; //return default key if field id not found
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed		  Field value or exception is thrown
	 */
	public function __get( $field ) {

		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'fields', 'title', 'options_pages' ), true ) ) {
			return $this->{$field};
		}
		if ( 'option_metabox' === $field ) {
			return $this->option_fields();
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

// Get it started
$my_Admin = new startimecampMain_Admin();
$my_Admin->hooks();

/**
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed		Option value
 */
function startimeMain_option( $key = '' ) {
	global $my_Admin;
	return cmb2_get_option( $my_Admin->get_option_key($key), $key );
}
