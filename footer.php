			<?php global $options, $pid, $prefix, $pmeta;
			// print_r($pmeta);
			 ?>
			<header id="head">
				<div class="container">
					<div class="row va">
						<div class="col-xs-12 col-sm-9 col-md-3">
							<?php
							$logo=(empty($options['gnrl']['header_logo_id'])) ? '<img src="'.$options['tpldir'].'/assets/img/logo.png" alt="logo" class="header__center-logo-img">' : remove_width_attribute(wp_get_attachment_image( $options['gnrl']['header_logo_id'], 'full', false, array('class'=>'logo') ));
							if(!is_front_page()){ ?>
								<?php if(function_exists('icl_get_languages')){
									$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
								} else {
									$home_url='/';
								} ?>
								<a href="<?php echo $home_url; ?>">
									<?= $logo ?>
								</a>
							<?php } else { ?>
								<?= $logo ?>
							<?php } ?>
						</div>
						<div class="col-xs-12 col-md-7 col-lg-5 col-lg-offset-1">
							<nav>
								<?php startimecamp_nav(); ?>
							</nav>
						</div>
						<div class="col-xs-12 col-sm-2  col-lg-offset-1">
							<div class="vimp">
								<div class="cnt">
									<ul class="">
										<li>
											<p class="text-center bold">Одесса</p>
										</li>
										<li>
											<p class="text-center">Отдел продаж</p>
											<a href="tel:+380487725787" class="phone-vimp"><?=$options['socl']['header_phone']?></a>
										</li>
										<li>
											<p class="text-center"><?=$options['socl']['address']?></p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<footer id="foot">
				<div class="container">
					<div class="row js-child-height">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 va-middle">
							<nav>
								<?php startimecamp_footer_nav(); ?>
							</nav>
							<div class="partner">
								<div class="f-part-cont">
									<div class="foot-img-cont">
										<a href="http://startime.ua">
											<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=$options['tpldir']?>/assets/img/part1.png" target="_blank" alt="Start Time">
										</a>
									</div>
									<a class="fimca" href="http://startime.ua" target="_blank">Продюсерский центр<br>startime</a>
								</div>
								<div class="f-part-cont">
									<div class="foot-img-cont">
										<a href="http://happytime.od.ua">
											<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=$options['tpldir']?>/assets/img/part_2.png" target="_blank" alt="Happy Time">
										</a>
									</div>
									<a class="fimca" href="http://happytime.od.ua" target="_blank">детский сад<br>happytime</a>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2 va-middle">
							<div class="star-cont">
								<img class="b-lazy star_gb" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=$options['tpldir']?>/assets/img/star_footer.png" alt="Happy Time">
								<!-- <img src="img/star_footer.png" alt="star" class="star_gb"> -->
								<ul class="social">
									<?php if(!empty($options['socl']['vk'])) { ?>
										<li><a href="<?=$options['socl']['vk']?>" target="_blank" class="icon-vk"></a></li>
									<?php } ?>
									<?php if(!empty($options['socl']['facebook'])) { ?>
										<li><a href="<?=$options['socl']['facebook']?>" target="_blank" class="icon-fb"></a></li>
									<?php } ?>
									<?php if(!empty($options['socl']['instagram'])) { ?>
										<li><a href="<?=$options['socl']['instagram']?>" target="_blank" class="icon-instagram"></a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-5">
							<div class="row tcont">
								<div class="cls">
									<div class="time time-nopd fullres">
										<h5>Отдел продаж</h5>
									</div>
									<div class="contact fullres">
										<h5>контакты</h5>
									</div>
								</div>
								<div class="time">
									<p class="icon icon-time">	</p>
									<div>
										<p class="cerlce">пн</p>
										<p class="cerlce">вт</p>
										<p class="cerlce">ср</p>
										<p class="cerlce">чт</p>
										<p class="cerlce">пт</p>
										<p class="digit">9:00 - 18:00</p>
									</div>
									<div class="yellow">
										<p class="cerlce">сб</p>
										<p class="cerlce">вс</p>
										<p class="digit cс">9:00 - 18:00</p>
									</div>
								</div>
								<div class="contact">
									<p class="addr"><?=str_replace('\"', '"', $options['socl']['address'])?></p>
									<?php
									$ph_arr = explode(',', $options['socl']['footer_phones'] );
									if(!empty($options['socl']['footer_phones'])){
										foreach($ph_arr as $ph){
											echo '<a href="tel:'.preg_replace("/[^A-Za-z0-9+]/", '', $ph).'">'.$ph.'</a>';
										}
									}
									?>
								</div>
							</div>
							<div class="row">
								<hr>
							</div>
							<div class="row tcont">
								<div class="time time-nopd fullres">
									<h5>Лагерь</h5>
								</div>
								<div class="contact fullres">
									<h5>контакты</h5>
								</div>
								<div class="cls"></div>
								<div class="time time phonepad">
									<p class="icon icon-time ">	</p>
									<div>
										<p class="cerlce">пн</p>
										<p class="cerlce">вт</p>
										<p class="cerlce">ср</p>
										<p class="cerlce">чт</p>
										<p class="cerlce">пт</p>
										<p class="cerlce">сб</p>
										<p class="cerlce">вс</p>
										<p class="digit c"> КРУГЛОСУТОЧНО  <br>
										<?php //<span class="cc">ПОСЕЩЕНИЕ РОДИТЕЛЕЙ с 14:00 ДО 16:00</span> </p>?>
									</div>
								</div>
								<div class="contact">
									<p class="addr"><?=str_replace('\"', '"', $options['socl']['address_camp'])?></p>
									<?php
									$ph_arr = explode(',', $options['socl']['footer_phones_camp'] );
									if(!empty($options['socl']['footer_phones_camp'])){
										foreach($ph_arr as $ph){
											echo '<a href="tel:'.preg_replace("/[^A-Za-z0-9+]/", '', $ph).'">'.$ph.'</a>';
										}
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<a href="#block1" class="dump_btn hidden icon-arr_up"></a>
		<div class="icon-load"></div>
		<div class="popup">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
						<div class="h"></div>
						<div class="form-cont">
							<?php
							$form=(empty($pmeta[$prefix.'form_sc'][0])) ? '[contact-form-7 title="Подать заявку" html_class="form form-feedback"]' : $pmeta[$prefix.'form_sc'][0];
							echo do_shortcode($form); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		global $options;
		wp_footer();
		if(!empty($options['advd']['custom_css'])){
			echo ($options['advd']['custom_css']!==' ') ? '<style>'.htmlspecialchars_decode($options['advd']['custom_css']).'</style>' : null;
		}
		if(!empty($options['advd']['custom_js'])){
			echo ($options['advd']['custom_js']!==' ') ? '<script>'.htmlspecialchars_decode($options['advd']['custom_js']).'</script>' : null;
		}
		if(!empty($options['advd']['custom_js_tag'])){
			echo ($options['advd']['custom_js_tag']!==' ') ? str_replace('\"', '"', $options['advd']['custom_js_tag']) : null;
		}
		if(!empty($options['advd']['custom_noscript'])){
			echo ($options['advd']['custom_noscript']!==' ') ? '<noscript>'.str_replace('\"', '"', $options['advd']['custom_noscript']).'</noscript>' : null;
		}
		?>
	</body>
</html>
