<?php /* Template Name: Страница О нас */ get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	$mainimg=wp_get_attachment_image($pmeta['_thumbnail_id'][0], 'full', false, array('class'=>'bg', 'alt'=>$ttl));
	$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="aboutus-top" class=" ">
				<?php
				if(!empty($mainimg)){
					echo remove_width_attribute($mainimg);
				} else { ?>
					<img src="<?=$options['tpldir']?>/assets/img/about_bg.jpg" class="bg">
				<?php }	?>
				<div class="container js-pad-top abcont">
					<div class="row">
						<div class="col-xs-12 col-sm-6 a-shap">
							<?php
							if(!empty($post->post_content)){
								$p_ttl=(!empty($pmeta[$prefix.'title'][0])) ? $pmeta[$prefix.'title'][0] : $ttl;
								if(!empty($p_ttl))	echo '<p class="h1">'.$p_ttl.'</p>';
								echo apply_filters( 'the_content', $post->post_content );
							} else {
							?>
								<p class="h1">
									 О звездном береге
								</p>
								<p class="text">Детский летний развивающий лагерь «Star Time» — это лицензированный творческий лагерь для детей. Расположен в живописном месте — на самом чистом пляже юга страны на берегу Черного моря.</p>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<div class="stext">
				<div class="container">
					<?php
					if(!empty($pmeta[$prefix.'conditions'][0])){
						$p_ttl=(!empty($pmeta[$prefix.'condtitle'][0])) ? $pmeta[$prefix.'condtitle'][0] : null;
						if(!empty($p_ttl))	echo '<p class="h1">'.$p_ttl.'</p>';
						echo apply_filters( 'the_content', $pmeta[$prefix.'conditions'][0] );
					} else { ?>
					<p class="h1">
						Условия проживания
					</p>
					<p>Комфортабельный пансионат «Солнечная поляна» находится в Московской области, городе Звенигороде, на берегу р. Москва.</p>
					<h2>НОМЕРА:</h2>
					<p>Проживание в комфортабельных 2-х, 3-х и 4-х местных номерах.</p>
					<p>Душ и туалет в номере или на блок из 2-х номеров.</p>
					<p>Проживающим предоставляются: постельное белье, полотенце.</p>
					<p>В общем пользовании участников лагеря находятся несколько кухонь с холодильниками, посудой и чайниками, а так же стиральная машина. </p>
					<?php }	?>
				</div>
				<?php
				$slider1=maybe_unserialize($pmeta[$prefix.'slider1'][0]);
				if(!empty($slider1)){
				?>
				<div class="slider">
					<?php
					// $sliderhtml=''
					foreach ($slider1 as $key => $value) {
						// $sliderhtml.='<div class="slide">'.wp_get_attachment_image_src($key, "about-slider-img").'</div>';
						$meta=wp_get_attachment_metadata($key, false);
						$src=wp_get_attachment_image_url($key, 'about-slider-img', $meta);
						printf('<div class="slide"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="%s" data-srcset="%s" data-sizes="%s"></div>',
							$src,
							wp_get_attachment_image_srcset($key, 'about-slider-img', $meta),
							wp_get_attachment_image_sizes($key, 'about-slider-img', $meta)
						);
					}
					?>
				</div>
				<?php } ?>
				<div class="container">
					<?php
					if(!empty($pmeta[$prefix.'infrastructure'][0])){
						echo apply_filters( 'the_content', $pmeta[$prefix.'infrastructure'][0] );
					} else { ?>
					<h2>ИНФРАСТРУКТУРА:</h2>
					<p>Территория пансионата является закрытой и находится под видеонаблюдением.</p>
					<p>Территория обладает разнообразным рельефом и пространствами для прогулок, отдыха и спортивных игр.</p>
					<p>Открыты для занятий футбольное, волейбольное и баскетбольное поля.</p>
					<p>Каждый год детей радуют вольеры с оленями и лебедями.</p>
					<p>Пансионат располагает медицинским центром, в котором участники могут получить своевременную медицинскую помощь. На территории свободная зона wi-fi.</p>
					<?php }	?>
				</div>
				<?php
				$slider2=maybe_unserialize($pmeta[$prefix.'slider2'][0]);
				if(!empty($slider2)){
				?>
				<div class="slider">
					<?php
					// $sliderhtml=''
					foreach ($slider2 as $key => $value) {
						// $sliderhtml.='<div class="slide">'.wp_get_attachment_image_src($key, "about-slider-img").'</div>';
						$meta=wp_get_attachment_metadata($key, false);
						$src=wp_get_attachment_image_url($key, 'about-slider-img', $meta);
						printf('<div class="slide"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="%s" data-srcset="%s" data-sizes="%s"></div>',
							$src,
							wp_get_attachment_image_srcset($key, 'about-slider-img', $meta),
							wp_get_attachment_image_sizes($key, 'about-slider-img', $meta)
						);
					}
					?>
				</div>
				<?php } ?>
				<div class="container">
					<?php
					if(!empty($pmeta[$prefix.'add_entertainment'][0])){
						echo apply_filters( 'the_content', $pmeta[$prefix.'add_entertainment'][0] );
					} else { ?>
						<p>Участники могут воспользоваться крытым бассейном за дополнительную плату. Также из дополнительных платных развлечений на территории имеются картинг, настольный теннис, бильярд, возможность прокататься на лошадях.</p>
						<p>Для занятий лагеря предоставляются специальные учебные залы и холлы для отдыха.</p>
					<?php }	?>
					<div class="row">
						<div class="col-xs-12">
							<p class="h1">
								Об организаторе
							</p>
						</div>
						<div class="col-xs-12 col-sm-6">
							<?php
							if(!empty($pmeta[$prefix.'about_org'][0])){
								echo apply_filters( 'the_content', $pmeta[$prefix.'about_org'][0] );
							} else { ?>
								<p>
									Организатор профлагеря - Центр тестирования и развития "Гуманитарные технологии". Это признанный лидер в области профориентации и карьерного развития молодежи. Передовые разработки Центра направлены на диагностику и формирование компетенций учащихся. Цель разработок - помощь в подготовке высокоэффективных профессионалов своего дела. Приоритеты Центра подтверждаются наличием авторских свидетельств и патентов.
								</p>
							<?php }	?>
						</div>
						<div class="col-xs-12 col-sm-6 text-center">
							<img class="b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=$options['tpldir']?>/assets/img/stdecor.jpg" alt="stdecor">
						</div>
					</div>
				</div>
			</div>
			<?php get_template_part( 'assets/php/blocks/block', 'docs' ); ?>
			<?php
			// <section id="test">
			// 	<div class="container">
			// 		<div class="row">
			// 			<div class="col-xs-12 col-sm-9">
			// 				<p class="h1">
			// 					Не знаешь, какую смену выбрать?
			// 				</p>
			// 				<p>Пройди бесплатный тест на сайте и получи ответ.</p>
			// 			</div>
			// 			<div class="col-xs-12 col-sm-3 text-right">
			// 				<a href="$options['tpldir']/assets/files/price.pdf" download class="btn btn-yellow openmodal">Пройти тест</a>
			// 			</div>
			// 		</div>
			// 	</div>
			// </section>
			?>
		<?php endwhile; ?>

		<?php else: ?>
			<section>
				<article>
					<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
				</article>
			</section>
		<?php endif; ?>
	</main>

<?php get_footer(); ?>
