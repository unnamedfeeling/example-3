<?php /* Template Name: Страница событий */ get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="events" class="js-pad-top">
				<div class="container">
					<h1>Концерты. Собрания. отдых</h1>
					<div class="row">
						<div class="events_loop">
							<div class="events_ell">
								<div class="events_bg"></div>
								<div class="col-sm-4 col-md-2">
									<img src="<?=$options['tpldir']?>/assets/img/events/event-p1.jpg" alt="event-p1">
								</div>
								<div class="col-sm-6 col-md-4 events_pd">
									<p class="events_name">
										Галаконцерт продюсерсокго<br>центра StarTime
									</p>
								</div>
								<div class=" col-sm-2 events_pd">
									<p class="events_date">03.07.2017 <br>
										<span class="events_time">19:30</span>
									</p>
								</div>
								<div class="col-sm-12 col-md-3 events_pd text-right">
									<div class="btn btn-purple js-event-modal">
										регистрация
									</div>
								</div>
							</div>
							<div class="events_ell">
								<div class="events_bg"></div>
								<div class="col-sm-4 col-md-2">
									<img src="<?=$options['tpldir']?>/assets/img/events/event-p2.jpg" alt="event-p1">
								</div>
								<div class="col-sm-6 col-md-4 events_pd">
									<p class="events_name">
										Галаконцерт продюсерсокго<br>центра StarTime
									</p>
								</div>
								<div class=" col-sm-2 events_pd">
									<p class="events_date">03.07.2017 <br>
										<span class="events_time">19:30</span>
									</p>
								</div>
								<div class="col-sm-12 col-md-3 events_pd text-right">
									<div class="btn btn-purple js-event-modal">
										регистрация
									</div>
								</div>
							</div>
						</div>
						<div class="events_bottom">
							<div class="col-sm-12 col-md-11 text-right">
								<div class="btn btn-green">Архив событий</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="medpop" class="hid">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-3 medpop_cont medpop_cont-1 text-center">
							<p class="medpop_date">
								03.07.2017
								<span class="time">19:30</span>
							</p>
						</div>
						<div class="col-xs-12 col-sm-9 medpop_cont">
							<div class="medpop_top">
								<p class="medpop_beg">Предстоящие события</p>
								<div class="medpop_btn medpop_btn-close icon-mod_close">
								</div>
							</div>
							<div class="medpop_h1">
								Галаконцерт продюсерсокго
								центра StarTime
							</div>
							<div class="medpop_poster">
								<img src="<?=$options['tpldir']?>/assets/img/pop-poster.jpg" class="" alt="medpop_poster">
								<div class="btn btn-purple medpop_btn-reg">Регистрация</div>
							</div>
							<div class="medpop_discr">
								<p>С ПРАЗДНИКОМ, МИЛЫЕ ДЕВУШКИ!</p>
								<p>
									Артисты детского развлекательного телепроекта "Зуб Мудрости" вместе с Русланом Костовым поздравляют очаровательных девушек, мам, бабушек с международным праздником!
									<br>

									Специально к 8 марта премьера выпуска! Открываем и делимся на странице: <a href="https://www.youtube.com/watch?v=fqbeoEUGU7I" target="_blank">https://www.youtube.com/watch?v=fqbeoEUGU7I</a>
								</p>

							</div>
						</div>
					</div>
				</div>
			</section>
		<?php endwhile; ?>

		<?php else: ?>
			<section>
				<article>
					<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
				</article>
			</section>
		<?php endif; ?>
	</main>

<?php get_footer(); ?>
