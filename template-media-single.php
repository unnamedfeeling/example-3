<?php /* Template Name: Страница медиа (1) */ get_header(); ?>
	<?php
	global $options, $pid, $prefix, $pmeta;
	$pid=$post->ID;
	$ttl=get_the_title();
	$prefix='startimecamp_';
	$pmeta=get_post_meta($pid, '', false);
	$pthumb=wp_get_attachment_image_url($pmeta['_thumbnail_id'][0], 'full');
	$bgc=(!empty($pmeta[$prefix.'block_col'][0])) ? 'style="background:'.$pmeta[$prefix.'block_col'][0].'"' : '';
	$tcol=(!empty($pmeta[$prefix.'txt_col'][0])) ? 'style="color:'.$pmeta[$prefix.'txt_col'][0].'"' : '';
	// print_r($bgc);
	// print_r($pmeta);
	?>
	<main class="content" role="main" aria-label="Content">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="media" class="js-pad-top">
				<div class="container">
				<h1 class="blue text-center">Фото</h1>

					<div id="media-slick-cont">

					</div>

				</div>
			</section>
			<div class="slick-pop">
				<div class="container">

					<div class="h"></div>

					<div class="scont">
						<div class="scont_btn-close icon-mod_close">
						</div>
						<div id="media-slick-2">
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_1.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/1.jpg" alt="albom">
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_2.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/2.jpg" alt="albom">
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_3.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/3.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_4.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/4.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_5.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/5.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_6.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/6.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_7.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/7.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_8.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/8.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_9.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/9.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_10.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/10.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_11.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/11.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_12.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/12.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_13.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/13.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_14.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/14.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_15.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/15.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_16.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/16.jpg" alt="albom" >
							</div>
							<div class="slide">
								<img data-mini="<?=$options['tpldir']?>/assets/img/slide/albom/min_17.jpg" data-lazy="<?=$options['tpldir']?>/assets/img/slide/albom/17.jpg" alt="albom" >
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>

		<?php else: ?>
			<section>
				<article>
					<h1><?php _e( 'Тут ничего нет. Печаль 8(', 'startimecamp' ); ?></h1>
				</article>
			</section>
		<?php endif; ?>
	</main>

<?php get_footer(); ?>
