<?php
get_header();
global $options;
$ecnt=wp_count_posts( 'event' );
$ttl=(!empty($options['gnrl']['evt_title'])) ? $options['gnrl']['evt_title'] : 'Концерты. Собрания. Отдых';
?>
<main class="content" role="main" aria-label="Content">
	<section id="events" class="js-pad-top" data-page="1">
		<div class="container">
			<h1><?=$ttl?></h1>
			<div class="row">
				<div class="events_loop">
					<?php if (have_posts()): while (have_posts()) : the_post();
						get_template_part('loop', 'event');
					endwhile;
					else : ?>
						<div class="noposts">Не найдено ни одного события!<br>Добавьте события!</div>
					<?php endif; ?>
				</div>
				<?php
				if(wp_count_posts( 'event' )>'6'){
				?>
				<div class="events_bottom">
					<div class="col-sm-12 col-md-11 text-right">
						<div class="btn btn-green js-get-more-evt">Архив событий</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<section id="medpop" class="hid"></section>
</main>
<?php get_footer(); ?>
